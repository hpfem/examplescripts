import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import scipy.sparse.linalg
import scipy.linalg

import waveequation1D_SCM

l = 1
p = 8
nx = 8
dx = l / nx

cut = (nx - 1) * dx + 0.1 * dx
alpha = 1e-8

frequency = 4
sigma = 0.03

fx, ft = waveequation1D_SCM.ricklersWavelet(sigma, frequency)

K, M, Fx, X = waveequation1D_SCM.assemble(l, nx, p, fx, cut, alpha)

eigvals = scipy.linalg.eigh(K.todense(), M.todense(), eigvals_only=True)
critical = 2.0 / np.sqrt(np.max(eigvals))

T = 2
m = 1.001 #0.999
dt = m * critical
nt = int(np.ceil(T / dt))
T = nt * dt

print( "Time integration ... ", flush=True )

factorized = scipy.sparse.linalg.splu( M )

u = np.zeros( ( nt + 1, nx * p + 1 ) )

for i in range( 2, nt + 1 ):
    u[i] = factorized.solve( M * ( 2 * u[i - 1] - u[i - 2] ) + dt**2 * ( Fx * ft( i * dt ) - K * u[i - 1] ) )

print( "Plotting ... ", flush=True )

# Plot animation
figure, ax = plt.subplots(figsize=(9, 7))
ax.set_xlim(0, l)
ax.set_ylim(-0.5, 2.1)
line,  = ax.plot(0, 0) 
line.set_xdata(X)

ax.plot([0, l],[1, 1], '--b')
ax.plot([cut, cut],[-0.5, 2.1], '--r')
ax.plot(X, [0] * len(X), marker='|', color='black')
ax.plot(np.linspace(0, l, nx + 1), np.zeros(nx + 1), marker='.', color='black')
ax.set_title(f'dt = {m} x critical time step ({nt} time steps)')
ts = ax.text(.01, .99, 'time step 0', ha='left', va='top', transform=ax.transAxes)

def prepareFrame(i):
    ts.set_text(f'time step {round(i / dt)}')
    line.set_ydata( u[round(i / dt)] )
    return line,

animationSpeed = 1
animationFPS = 30

frames = np.linspace(0, T, round( T * animationFPS / animationSpeed))

animation = anim.FuncAnimation(figure, func=prepareFrame, frames=frames, interval=1000/animationFPS)
#animation.save('cut.gif') 
                      
plt.show()
