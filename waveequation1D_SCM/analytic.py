import sympy
import numpy
import matplotlib.pyplot as plt

x, eps, alpha = sympy.symbols('x eps alpha', positive=True)

M = sympy.integrate( x**2, (x, 0, eps)) + alpha*sympy.integrate( x**2, (x, eps, 1))
K = sympy.integrate( 1**2, (x, 0, eps)) + alpha*sympy.integrate( 1**2, (x, eps, 1))

# 3*(alpha*(eps - 1) - eps)/(alpha*(eps**3 - 1) - eps**3)
lmbda = K / M

print(f'K = {K}\nM = {M}\nK / M = {lmbda}')

samples = numpy.logspace(-16, 0, 20)
critical = [[float(lmbda.subs(alpha, a).subs(eps, c)) for a in samples] for c in samples]
X, Y = numpy.meshgrid(samples, samples, indexing='ij')

fig, ax = plt.subplots(figsize=(5.5, 7))
cs = ax.contourf(numpy.log10(X), numpy.log10(Y), numpy.log10(critical), levels=16)
plt.xlabel('log10(eps)')
plt.ylabel('log10(alpha)')
fig.colorbar(cs, label='log10(K)', orientation='horizontal')
plt.show()
