import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg

import waveequation1D_SCM

l = 1
p = 8
nx = 8
dx = l / nx

frequency = 4
sigma = 0.03

fx, ft = waveequation1D_SCM.ricklersWavelet(sigma, frequency)

resolution = 40 #1000

cuts = np.logspace(-6, -1e-6, resolution)
alphas = 10.0**np.arange(0, -13, step=-2)

critical=[]

# Compute critical time steps
for alpha in alphas:
    print('alpha =', alpha)
    critical.append([])
    for cut in cuts * dx + (nx - 1) * dx:
        K, M, Fx, X = waveequation1D_SCM.assemble(l, nx, p, fx, cut, alpha)
        eigvals = scipy.linalg.eigh(K.todense(), M.todense(), eigvals_only=True)
        critical[-1].append(2.0 / np.sqrt(np.max(eigvals)))
   
# Plot critical time steps over cut ratios
plt.figure(figsize=(9,7))

for alpha, criticalI in zip(alphas, critical):
    plt.loglog(cuts, criticalI, label="alpha = " + str(alpha))
 
plt.legend()
plt.xlabel ('Volume fraction [-]')
plt.ylabel ('Critical time step [s]')
plt.show()

# Plot smallest critical time steps over alpha
plt.figure(figsize=(9,7))
plt.loglog(alphas, [np.min(criticalI) for criticalI in critical], 'bx-')
plt.loglog([1e-11, 2e-10, 2e-10, 1e-11], np.array([1e-11, 1e-11, 2e-10, 1e-11])**(1/3)/200, 'b')
plt.text(3e-10, 1.6e-06, f'{1}', color='blue', fontsize=14)
plt.text(4e-11, 7.3e-07, f'{3}', color='blue', fontsize=14)
plt.xticks(alphas)
plt.xlabel ('alpha value [-]')
plt.ylabel ('smallest critical time step [s]')
plt.show()
