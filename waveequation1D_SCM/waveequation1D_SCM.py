import numpy as np
import scipy.sparse
import scipy.interpolate

# Gauss-Lobatto points and weights taken from here:
# https://colab.research.google.com/github/caiociardelli/sphglltools/blob/main/doc/L3_Gauss_Lobatto_Legendre_quadrature.ipynb
def lgP (n, xi):
  if n == 0:
    return np.ones (xi.size)
  
  elif n == 1:
    return xi

  else:
    fP = np.ones (xi.size); sP = xi.copy (); nP = np.empty (xi.size)
    for i in range (2, n + 1):
      nP = ((2 * i - 1) * xi * sP - (i - 1) * fP) / i
      fP = sP; sP = nP

    return nP
    
def GLL(n, epsilon = 1e-15):
  if n < 2:
    print('Error: n must be larger than 1')
  
  else:
    x = np.empty (n)
    w = np.empty (n)
    
    x[0] = -1; x[n - 1] = 1
    w[0] = w[0] = 2.0 / ((n * (n - 1))); w[n - 1] = w[0];
    
    n_2 = n // 2
    
    dLgP  = lambda n, xi: n * (lgP (n - 1, xi) - xi * lgP (n, xi)) / (1 - xi ** 2)
    d2LgP = lambda n, xi: (2 * xi * dLgP (n, xi) - n * (n + 1) * lgP (n, xi)) / (1 - xi ** 2)
    d3LgP = lambda n, xi: (4 * xi * d2LgP (n, xi) - (n * (n + 1) - 2) * dLgP (n, xi)) / (1 - xi ** 2)                             
                                      
    for i in range (1, n_2):
      xi = (1 - (3 * (n - 2)) / (8 * (n - 1) ** 3)) *\
           np.cos ((4 * i + 1) * np.pi / (4 * (n - 1) + 1))
      
      error = 1.0
      
      while error > epsilon:
        y  =  dLgP (n - 1, xi)
        y1 = d2LgP (n - 1, xi)
        y2 = d3LgP (n - 1, xi)
        
        dx = 2 * y * y1 / (2 * y1 ** 2 - y * y2)
        
        xi -= dx
        error = abs (dx)
        
      x[i] = -xi
      x[n - i - 1] =  xi
      
      w[i] = 2 / (n * (n - 1) * lgP (n - 1, x[i]) ** 2)
      w[n - i - 1] = w[i]
    
    if n % 2 != 0:
      x[n_2] = 0;
      w[n_2] = 2.0 / ((n * (n - 1)) * lgP (n - 1, np.array (x[n_2])) ** 2)
    
  return np.array(x), np.array(w)

def ricklersWavelet(sigmaS, frequency):
    t0 = 1.0 / frequency
    sigmaT = 1.0 / ( 2.0 * np.pi * frequency )

    fx = lambda x : 0.25 / np.sqrt( 2 * np.pi * sigmaS**2 ) * np.exp( -x**2 / ( 2 * sigmaS**2 ) )
    ft = lambda t : -( t - t0 ) / ( np.sqrt( 2 * np.pi ) * sigmaT**3 ) * np.exp( -( t - t0 )**2 / ( 2 * sigmaT**2 ) )

    return fx, ft

def assemble(l, nx, p, fx, cut, epsilon):

    rho = 1
    E = 1
    dx = l / nx

    # Quadrature
    gllCoordinates, gllWeights = GLL(p + 1)
    lagrange = lambda i : scipy.interpolate.lagrange(gllCoordinates, np.identity(p + 1)[i])

    shapesDiff0 = [lagrange(i) for i in range( p + 1 )]    
    shapesDiff1 = [np.polyder(shape) for shape in shapesDiff0];

    mapX = lambda ielement, r: (ielement + r / 2.0 + 0.5) * dx
     
    cutindex = int(cut / dx)
    cutlocal = (cut - cutindex * dx) / dx * 2.0 - 1.0
   
    # Data arrays
    row  = np.zeros( (p + 1) * (p + 1) * nx, dtype=np.uint )
    col  = np.zeros( (p + 1) * (p + 1) * nx, dtype=np.uint )
    Mdata = np.zeros( (p + 1) * (p + 1) * nx )
    Kdata = np.zeros( (p + 1) * (p + 1) * nx )
    
    Fx = np.zeros( ( nx * p + 1, ) )
        
    # Assemble linear system
    for ielement in range( nx ):
        Me = np.zeros( (p + 1, p + 1) ) 
        Ke = np.zeros( (p + 1, p + 1) ) 
        Fe = np.zeros( (p + 1, ) ) 
        
        locationMap = np.arange(ielement * p, ielement * p + (p + 1))
        partitions = ((-1, 1), ) if ielement != cutindex else ((-1, cutlocal), (cutlocal, 1)) 
        coordinates, weights = np.polynomial.legendre.leggauss(p + 1)
          
        # Integrate stiffness matrix and force vector
        for (r0, r1) in partitions:
            for igauss, (xi, W) in enumerate(zip(coordinates, weights)):
                r = xi*(r1 - r0)/2 + (r1 + r0)/2
                x = (ielement + r / 2.0 + 0.5) * dx
                w = 1 / 4 * (r1 - r0) * dx * W
              
                N0 = np.array([shape(r) for shape in shapesDiff0])
                N1 = np.array([shape(r) for shape in shapesDiff1]) * 2 / dx
                     
                alpha = 1.0 if x <= cut else epsilon
                         
                # Add contributions
                Ke += np.outer( N1, N1 ) * E * alpha * w
                Fe += N0 * fx(x) * alpha * w
                #Me += np.outer(N0, N0) * rho * alpha * w
    
        if ielement != cutindex:
            coordinates, weights = gllCoordinates, gllWeights
            
        # Integrate mass matrix
        for (r0, r1) in partitions:
            for igauss, (xi, W) in enumerate(zip(coordinates, weights)):
                r = xi*(r1 - r0)/2 + (r1 + r0)/2
                x = (ielement + r / 2.0 + 0.5) * dx
                w = 1 / 4 * (r1 - r0) * dx * W
              
                N = np.array([shape(r) for shape in shapesDiff0])
                         
                alpha = 1.0 if x <= cut else epsilon
                         
                # Add contributions
                Me += np.outer(N, N) * rho * alpha * w
    
        elementSlice = slice((p + 1) * (p + 1) * ielement, (p + 1) * (p + 1) * (ielement + 1))
        
        row[elementSlice] = np.broadcast_to( locationMap, (p + 1, p + 1) ).T.ravel( )
        col[elementSlice] = np.broadcast_to( locationMap, (p + 1, p + 1) ).ravel( )
    
        Mdata[elementSlice] = Me.ravel( )
        Kdata[elementSlice] = Ke.ravel( )
        
        # Add to spatial rhs
        Fx[locationMap] += Fe
    
    M = scipy.sparse.coo_matrix( ( Mdata, (row, col) ) ).tocsc( )
    K = scipy.sparse.coo_matrix( ( Kdata, (row, col) ) ).tocsc( )

    X = [mapX(i, r) for i in range(nx) for r in gllCoordinates[:-1]] + [mapX(nx - 1, gllCoordinates[-1])]

    return K, M, Fx, X
    