import numpy
import matplotlib.pyplot as plt
import scipy.optimize as opt
import scipy.interpolate as ip

# Consider some function f(x) and and the functional
#    F(u) = \int_x 1/2*(u - f)^2 * (x + 1) dx 
#    dF(u; d) = \int_x (u - f)*d * (x + 1) dx  = <(u - f) * (x + 1), d>
#
# Choose a B-Spline basis and run scipy minimizaion

f = lambda x : numpy.sin(30.0*x) + x + 2.0
#f = lambda x : numpy.sin(6.0*x) + x + 2.0
#f = lambda x : (x - 0.5)**2 + 0.5

# ---------- Basis definition -------------
class BSplineBasis:
    def __init__(self, nspans, degree=1, continuity_=None):
        continuity = continuity_ if continuity_ is not None else degree - 1
        assert(nspans > 0 and degree > 0 and continuity >= 0 and continuity < degree)
        self.nelements, self.rep = nspans, degree - continuity
        self.ndof = (nspans - 1) * self.rep + (degree + 1)
        self.locationmaps = [numpy.arange(degree + 1) + i * self.rep for i in range(nspans)]
        
        t = [[x] * self.rep for x in numpy.linspace(0, 1, nspans + 1)]
        t = [[0.0] * (continuity + 1)] + t + [[1.0] * (continuity + 1)]
        t = numpy.concatenate(t)

        self.basis = [ip.BSpline(t, numpy.eye(self.ndof)[i], degree) for i in range(self.ndof)]
            
    def degree(self, ielement):
        return len(self.locationmaps[0]) - 1
        
    def mapDetJ(self, ielement, r):
        return ((r + 1.0) / 2.0 + ielement) / self.nelements, 2.0 / self.nelements
    
    def evaluate(self, ielement, r):
        x = self.mapDetJ(ielement, r)[0]
        return numpy.array([self.basis[i](x) for i in self.locationmaps[ielement]])

# Piecewise linear
#basis = BSplineBasis(1)

# 8 knot spans, p = 2, C^0 continuous
#basis = BSplineBasis(8, 2, 0) 

# 4 knot spans, p = 6, C^4 continuous
basis = BSplineBasis(4, 6, 4) 

#basis = BSplineBasis(4, 2, 0) 
#basis = BSplineBasis(8, 1, 0) 

# -------------- Assembly -----------------
def assemble(kernel, target, quadratureOffset=10):
    kernel.target = target
    for ielement, map in enumerate(basis.locationmaps):
        for r, w in zip(*numpy.polynomial.legendre.leggauss(basis.degree(ielement) + quadratureOffset)):
            x, J = basis.mapDetJ(ielement, r)
            kernel(r, x, w * J, map, basis.evaluate(ielement, r))
    return kernel.target

def objective(dofs):
    def kernel(r, x, detJ, map, N):
        kernel.target += 0.5*(numpy.dot(dofs[map], N) - f(x))**2
    return assemble(kernel, 0.0)
      
def gradient(dofs):
    def kernel(r, x, detJ, map, N):
        kernel.target[map] += N * (numpy.dot(dofs[map], N) - f(x)) 
    return assemble(kernel, numpy.zeros(len(dofs)))
   
def massMatrix( ):
    def kernel(r, x, detJ, map, N):
        kernel.target[numpy.ix_(map, map)] += numpy.outer(N, N)
    return assemble(kernel, numpy.zeros((basis.ndof, basis.ndof)))
 
def projectedGradient(dofs):
    return numpy.linalg.solve(massMatrix(), gradient(dofs))

# ------------ Optimization ---------------
d0 = numpy.zeros(basis.ndof)

method ='L-BFGS-B' #'BFGS', 'L-BFGS-B'
options = { }#{'maxiter' : 20}#{'disp': True}

F1, F2 = [], []

def callback1(v):
    F1.append(objective(v))
def callback2(v):
    F2.append(objective(v))

result1 = opt.minimize(objective, d0, jac=gradient, 
    method=method, options=options, callback=callback1)
result2 = opt.minimize(objective, d0, jac=projectedGradient, 
    method=method, options=options, callback=callback2)
    
offset = 0.1

udofs = result1.x
uoffsetted = udofs - offset

offsetDiscrete = gradient(uoffsetted)
offsetPojected = projectedGradient(uoffsetted)

# ----------- Postprocessing --------------

print("ndof = " + str(basis.ndof))
print("nelements = " + str(basis.nelements))

print("\ngradient:\n    nit = " + str(result1.nit) + "\n    F   = " + str(result1.fun) ) 
print("\nprojected gradient:\n    nit = " + str(result2.nit) + "\n    F   = " + str(result2.fun) ) 

values = [], [], [], []

for ielement, map in enumerate(basis.locationmaps):
    rvalues = numpy.linspace(-1.0, 1.0, basis.degree(ielement) + 30)
    
    for v in values:
        v.append(numpy.zeros(rvalues.shape))
        
    for i, r in enumerate(rvalues):
        shapes = basis.evaluate(ielement, r)
        values[0][-1][i] = basis.mapDetJ(ielement, r)[0]
        values[1][-1][i] = numpy.dot(shapes, -offsetDiscrete[map])
        values[2][-1][i] = numpy.dot(shapes, -offsetPojected[map])
        values[3][-1][i] = numpy.dot(shapes, udofs[map])

X, V0, V1, VF = [numpy.concatenate(v) for v in values]

plt.figure(figsize=(14, 6))

plt.subplot(1, 2, 1)
plt.plot(X,f(X))
plt.plot(X,VF)
plt.plot(X,VF - offset)
plt.title("")
plt.xlabel('x')
plt.legend(['target function f = sin(6*x) + x + 2', 'B-spline approximation u^h', 'u^h - ' + str(offset)])

ylim = plt.subplot(1, 2, 2).get_ylim()
plt.plot(X,V0)
plt.plot(X,V1)
plt.plot(X, f(X) - VF)
plt.title("Gradient evaluated at f - " + str(offset) + " with different methods.")
plt.xlabel('x')
plt.ylim([ylim[0], ylim[1] + 0.25*(ylim[1] - ylim[0])])
plt.legend(['"discrete" gradient', 'projected gradient', 'continuous gradient u^h - f'])

plt.tight_layout()
plt.show()

plt.figure(figsize=(14, 6))
plt.subplot(1, 2, 1)
plt.semilogy(numpy.array(F1), 'x-')
plt.semilogy(numpy.array(F2), 'x-')
plt.title("Absolute objective function value: F = 1/2 int_x (u(x) - f(x))^2 dx.")
plt.xlabel('L-BFGS iteration')
plt.ylabel('Objective function value')
plt.legend(['"discrete" gradient', 'projected gradient'])

plt.subplot(1, 2, 2)
plt.semilogy(numpy.array(F1[:-1])-F2[-1], 'x-')
plt.semilogy(numpy.array(F2[:-1])-F2[-1], 'x-')
plt.title("Relative objective function compared to final value")
plt.xlabel('L-BFGS iteration')
plt.ylabel('F_i - F_n')
plt.legend(['"discrete" gradient', 'projected gradient'])


plt.tight_layout()
plt.show()

# ------------ Plot 2D picture for two dofs ---------------

#X, Y = numpy.meshgrid(numpy.linspace(0, 1, 50), numpy.linspace(0, 1, 50), indexing='ij')
#
#U = X * 0
#
#for i, j in numpy.ndindex(50, 50):
#    U[i, j] = objective(numpy.array([X[i, j], Y[i, j]]))
#
#g1 = gradient(numpy.array([0.2, 0.6]))
#g2 = projectedGradient(numpy.array([0.2, 0.6]))
#
#plt.contour(X, Y, U, levels=64)
#plt.arrow(0.2, 0.6, -g1[0], -g1[1], color='r', length_includes_head=True, width= 0.002, head_width=0.05, head_length=0.05)
#plt.arrow(0.2, 0.6, -g2[0], -g2[1], color='b', length_includes_head=True, width= 0.002, head_width=0.05, head_length=0.05)
#
#plt.title("Objective function values for two coefficients of linear interpolation")
#plt.xlabel('uhat_0')
#plt.ylabel('uhat_1')
#plt.legend(['discrete gradient', 'projected gradient'])
#plt.show()

