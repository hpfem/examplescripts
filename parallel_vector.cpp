#include <vector>
#include <numeric>
#include <cstddef>
#include <iostream>
#include <cmath>
#include <chrono>
#include <omp.h>

// compile without OMP: g++ -std=c++20 -Ofast -o test test.cpp
// compile with OMP:    g++ -std=c++20 -Ofast -o test test.cpp -fopenmp

#define NOINLINE __attribute__ ((noinline)) 

auto NOINLINE createData(size_t size1, size_t size2)
{
    std::vector<std::vector<double>> data;
 
    data.resize(size1); 
    
    for(size_t i = 0; i < data.size(); ++i)
    {
       size_t size2_ = size2 * (1 + 0.2 * std::sin(i / 100.0));
    
       data[i].resize(size2_);
    
       for(size_t j = 0; j < size2_; ++j)
       {
            data[i][j] = 10 * i + j;
       }
    }

    return data; 
}

// Some random computions
double compute(double v)
{
    return std::sqrt(std::exp(std::sin(v))) + std::pow(v, 0.32123) + 
         std::pow(v, -2.12423) + std::pow(v, -2.03412);
}

auto NOINLINE version1(const std::vector<std::vector<double>>& data)
{
    std::vector<double> result;

    for(const auto& vec : data)
    {
        for(auto entry : vec)
        {
            result.push_back(compute(entry));
        }
    }

    return result;
}

auto NOINLINE version2(const std::vector<std::vector<double>>& data)
{
    // Find first entry in result vector for each vector in data
    std::vector<size_t> offsets(data.size() + 1);
    
    offsets[0] = 0;

    #pragma omp parallel for schedule(dynamic, 512)
    for(size_t i = 0; i < data.size(); ++i)
    {
        offsets[i + 1] = data[i].size();
    }
    
    std::partial_sum(offsets.begin(), offsets.end(), offsets.begin());
    
    // Allocate result and do linearization / computation
    std::vector<double> result(offsets.back(), 0.0);

    #pragma omp parallel for schedule(dynamic, 1024)
    for(size_t i = 0; i < data.size( ); ++i)
    {
        for( size_t j = 0; j < data[i].size(); ++j)
        {
            result[offsets[i] + j] = compute(data[i][j]); 
        }
    }

    return result;
}

int main() 
{
    auto data = createData(50000, 1000);

    auto start = std::chrono::high_resolution_clock::now();    

    //auto result = version1(data);
    auto result = version2(data);

    auto stop = std::chrono::high_resolution_clock::now();

    auto seconds = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

    std::cout << "Took: " << seconds.count() << " ms" << std::endl;
}

