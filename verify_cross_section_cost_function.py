import numpy, functools

def integrate(f, starts, ends, nsubdivisions=None, degree=10):

    D = len(starts)
    ncells = [6] * D if nsubdivisions is None else nsubdivisions
    slices = [(None, ) * i + (slice(None), ) + (None, ) * (D - i - 1) for i in range(D)]

    c, w = numpy.polynomial.legendre.leggauss(degree)
    W = functools.reduce(numpy.multiply, [w[sliceI] for sliceI in slices])
    W = W.ravel() * numpy.prod([(b - a) / (2 * n) for a, b, n in zip(starts, ends, ncells)])

    value = 0
    for ijk in numpy.ndindex(*ncells):
        ci = [(c/2 + 1/2 + i) * (b - a) / n + a for i, n, a, b in zip(ijk, ncells, starts, ends)]  
        Ci = [C.ravel( ) for C in numpy.meshgrid(*ci, indexing='ij')]
        
        value += numpy.sum(f(*Ci) * W)
 
    return value

# -------------------
beta = lambda y, z: 1 - 2 * (y**2 + (1 - z)**2 < 0.3**2)
S = lambda tau: 0.5*tau**2 * (tau > 0)
dS = lambda tau: tau * (tau > 0)

p = 5
Tm = 0.5

Lpx = lambda T, y, z: numpy.array([integrate(lambda x: T(x, x*0 + yi, x*0 + zi)**p, [0], [1])**(1/p) for yi, zi in zip(y, z)])

objective = lambda T: integrate(lambda y, z: S(beta(y, z) * (Lpx(T, y, z) - Tm)), [0, 0], [1, 1])

def gradient(T, dT):
    def integrandYZ(y, z):
        values = []
        Lp_yz, beta_yz = Lpx(T, y, z), beta(y, z)
        for yi, zi in zip(y, z):
            def integrandX(x):
                Txyz, dTxyz = T(x, x*0 + yi, x*0 + zi), dT(x, x*0 + yi, x*0 + zi)
                return Txyz * numpy.abs(Txyz)**(p - 2) * dTxyz
            values = values + [integrate(integrandX, [0], [1])]
        return dS(beta_yz * (Lp_yz - Tm)) * beta_yz * Lp_yz**(1 - p) * numpy.array(values)
    return integrate(integrandYZ, [0, 0], [1, 1])
      
def finite(T, dT, eps=1e-5):
    return (objective(lambda x, y, z: T(x, y, z) + eps * dT(x, y, z)) - objective(T)) / eps
      
# -------------------
T = lambda x, y, z: x**2 + y**2 + z**2
dT = lambda x, y, z: numpy.sin(4*x + 0.3) * numpy.cos(3*y + 3.2) * numpy.sin(4.3*z - 0.9)

print("Finite difference : " + str(finite(T, dT)))
print("Analytical        : " + str(gradient(T, dT)))
