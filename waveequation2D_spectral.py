import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.sparse
import scipy.sparse.linalg
import scipy.interpolate
  
# Gauss-Lobatto points and weights taken from here:
# https://colab.research.google.com/github/caiociardelli/sphglltools/blob/main/doc/L3_Gauss_Lobatto_Legendre_quadrature.ipynb
def lgP (n, xi):
  if n == 0:
    return np.ones (xi.size)
  
  elif n == 1:
    return xi

  else:
    fP = np.ones (xi.size); sP = xi.copy (); nP = np.empty (xi.size)
    for i in range (2, n + 1):
      nP = ((2 * i - 1) * xi * sP - (i - 1) * fP) / i
      fP = sP; sP = nP

    return nP
    
def GLL(n, epsilon = 1e-15):
  if n < 2:
    print('Error: n must be larger than 1')
  
  else:
    x = np.empty (n)
    w = np.empty (n)
    
    x[0] = -1; x[n - 1] = 1
    w[0] = w[0] = 2.0 / ((n * (n - 1))); w[n - 1] = w[0];
    
    n_2 = n // 2
    
    dLgP  = lambda n, xi: n * (lgP (n - 1, xi) - xi * lgP (n, xi)) / (1 - xi ** 2)
    d2LgP = lambda n, xi: (2 * xi * dLgP (n, xi) - n * (n + 1) * lgP (n, xi)) / (1 - xi ** 2)
    d3LgP = lambda n, xi: (4 * xi * d2LgP (n, xi) - (n * (n + 1) - 2) * dLgP (n, xi)) / (1 - xi ** 2)                             
                                      
    for i in range (1, n_2):
      xi = (1 - (3 * (n - 2)) / (8 * (n - 1) ** 3)) *\
           np.cos ((4 * i + 1) * np.pi / (4 * (n - 1) + 1))
      
      error = 1.0
      
      while error > epsilon:
        y  =  dLgP (n - 1, xi)
        y1 = d2LgP (n - 1, xi)
        y2 = d3LgP (n - 1, xi)
        
        dx = 2 * y * y1 / (2 * y1 ** 2 - y * y2)
        
        xi -= dx
        error = abs (dx)
        
      x[i] = -xi
      x[n - i - 1] =  xi
      
      w[i] = 2 / (n * (n - 1) * lgP (n - 1, x[i]) ** 2)
      w[n - i - 1] = w[i]
    
    if n % 2 != 0:
      x[n_2] = 0;
      w[n_2] = 2.0 / ((n * (n - 1)) * lgP (n - 1, np.array (x[n_2])) ** 2)
    
  return np.array(x), np.array(w)
  
# Domain definition
lengths = [2, 2]
duration = 3

center = [1.0, 1.0]
radius = 0.4

domain = lambda x, y : (x - center[0])**2 + (y - center[1])**2 >= radius**2

# Coefficients
alpha = lambda x, y : 1.0 if domain( x, y ) else 1e-6

rho = lambda x, y : alpha( x, y )
E  = lambda x, y : alpha( x, y )

# Source function (Ricklers wavelet)
frequency = 4
t0 = 1.0 / frequency

sigmaT = 1.0 / ( 2.0 * np.pi * frequency )
sigmaS = 0.06

ft = lambda t : -(t - t0) / (np.sqrt(2 * np.pi) * sigmaT**3) * np.exp( -(t - t0)**2 / (2 * sigmaT**2) )
fx = lambda x, y : 10*np.exp( -(x**2 + y**2)/(2*sigmaS**2)) if domain(x, y) else 0.0
    
# Discretization
nelements = [12, 12]
nsteps = 2000
polynomialDegree = 5
quadratureOrder = polynomialDegree + 1

treedepth = polynomialDegree + 1
nseedpoints = polynomialDegree + 2
 
dt = duration / nsteps
dx = lengths[0] / nelements[0]
dy = lengths[1] / nelements[1]

# Choose method 
# 'FCM' : Equidistant Lagrange shape function coordiantes, Gauss-Legendre quadrature
# 'SEM' : Gauss-Lobatto Lagrange coordinates, Gauss-Lobatto mass matrix integration, no lumping for cut cells
# 'SCM' : Gauss-Lobatto Lagrange coordinates, Gauss-Lobatto mass matrix integration, HRZ lumping for cut cells
method = 'SCM'
     
# Prepare Lagrange polynomials
gllCoordinates, gllWeights = GLL(polynomialDegree + 1)
equidistantCoordinates = np.linspace(-1.0, 1.0, polynomialDegree + 1)

lagrangeCoords = equidistantCoordinates if method == 'FCM' else gllCoordinates
lagrangeValues = np.identity(polynomialDegree + 1)

lagrange = lambda i : scipy.interpolate.lagrange(lagrangeCoords, lagrangeValues[i])

shapesDiff0 = [lagrange(i) for i in range( polynomialDegree + 1 )]    
shapesDiff1 = [np.polyder(shape) for shape in shapesDiff0];
 
ndofelement = (polynomialDegree + 1)**2
ndofdirection = [nelements[0] * polynomialDegree + 1, nelements[1] * polynomialDegree + 1]
ndofglobal = ndofdirection[0] * ndofdirection[1]
    
print( "Assembling (" + str( ndofglobal ) + " dofs, " + str( nelements[0] * nelements[1] ) + " elements) ... ", flush=True )

# Allocate data structure for coordinate format
row   = np.zeros( ndofelement**2 * nelements[0] * nelements[1], dtype=np.uint )
col   = np.zeros( ndofelement**2 * nelements[0] * nelements[1], dtype=np.uint )
Kdata = np.zeros( ndofelement**2 * nelements[0] * nelements[1] )
Mdata = np.zeros( ndofelement**2 * nelements[0] * nelements[1] )

Fx = np.zeros( ndofglobal )

mapX = lambda ielement, r: (ielement + r / 2.0 + 0.5) * dx
mapY = lambda jelement, s: (jelement + s / 2.0 + 0.5) * dy

def generateQuadtree(ielement, jelement):
    begin, end, depth, partitions = 0, 1, 0, []
    tree = [((-1.0, 1.0), (-1.0, 1.0))]
    
    while begin != end and depth < treedepth:
        for (r0, r1), (s0, s1) in tree[begin:end]:
        
            # Create grid of points and evaluate embedded domain
            seedX = np.linspace(mapX(ielement, r0), mapX(ielement, r1), nseedpoints)
            seedY = np.linspace(mapY(jelement, s0), mapY(jelement, s1), nseedpoints)
            
            X, Y = np.meshgrid(seedX, seedY, indexing='ij')
            
            result = domain(X.ravel( ), Y.ravel( ))
            
            # Subdivide if some are inside and some outside                
            if not np.all(result) and not np.all(np.logical_not(result)):
                tree += [((r0, (r0 + r1)/2), (s0, (s0 + s1)/2)),
                         (((r0 + r1)/2, r1), (s0, (s0 + s1)/2)),
                         ((r0, (r0 + r1)/2), ((s0 + s1)/2, s1)),
                         (((r0 + r1)/2, r1), ((s0 + s1)/2, s1))]
            else:
                partitions.append(((r0, r1), (s0, s1))) 
                         
        begin, end, depth = end, len(tree), depth + 1
    
    return partitions + tree[begin:end]
       
def createLocationMap(ielement, jelement):
   dofIndicesI = np.arange(polynomialDegree + 1) + polynomialDegree * ielement
   dofIndicesJ = np.arange(polynomialDegree + 1) + polynomialDegree * jelement
   return np.add.outer(dofIndicesI * ndofdirection[1], dofIndicesJ).ravel( )
        
def createElementSlice(ielement, jelement):
    elementIndex = jelement * nelements[0] + ielement
    return slice(ndofelement**2 * elementIndex, ndofelement**2 * (elementIndex + 1))

# Assemble lumped mass matrix
for ielement in range( nelements[0] ):
    for jelement in range( nelements[1] ):
        Me = np.zeros( ( ndofelement, ndofelement ) ) 
        
        locationMap = createLocationMap(ielement, jelement)
        partitions = generateQuadtree(ielement, jelement)

        cellIsCut = len(partitions) > 1
        Mcell = 0.0

        for (r0, r1), (s0, s1) in partitions:
            
            if method == 'FCM' or cellIsCut:
                coordinates, weights = np.polynomial.legendre.leggauss( quadratureOrder )
            else:
                coordinates, weights = gllCoordinates, gllWeights
            
            # Map from integration cell to element
            rValues = [xi*(r1 - r0)/2 + (r1 + r0)/2 for xi in coordinates]
            sValues = [et*(s1 - s0)/2 + (s1 + s0)/2 for et in coordinates]
            
            # Evaluate Lagrange polynomials in both directions
            shapesRDiff0 = [np.array([shape(r) for shape in shapesDiff0]) for r in rValues]
            shapesSDiff0 = [np.array([shape(s) for shape in shapesDiff0]) for s in sValues]
            
            for igauss, (r, w0) in enumerate(zip(rValues, weights)):
                for jgauss, (s, w1) in enumerate(zip(sValues, weights)):
                    x, y = mapX(ielement, r), mapY(jelement, s)
                    weight = 1/16 * (r1 - r0) * (s1 - s0) * dx * dy * w0 * w1
                  
                    Mcell += rho(x, y) * weight
                  
                    # Compute tensor product and derivatives w.r.t. x and y
                    N = np.outer( shapesRDiff0[igauss], shapesSDiff0[jgauss] ).ravel( )
                                    
                    # Add contributions
                    Me += np.outer(N, N) * rho(x, y) * weight

        # Lump if cut
        if method == 'SCM' and cellIsCut:
            #c = 2 * Mcell / np.trace(Me)
            c = Mcell / np.trace(Me)
            Me = c * np.diag(np.diag(Me))

        Mdata[createElementSlice(ielement, jelement)] = Me.ravel( )      
       
# Assemble stiffness matrix and right hand side
for ielement in range( nelements[0] ):
    for jelement in range( nelements[1] ):
        Ke = np.zeros( ( ndofelement, ndofelement ) )
        Fe = np.zeros( ( ndofelement, ) )
        
        locationMap = createLocationMap(ielement, jelement)
        partitions = generateQuadtree(ielement, jelement)

        for (r0, r1), (s0, s1) in partitions:
            
            coordinates, weights = np.polynomial.legendre.leggauss(quadratureOrder)
            
            # Map from integration cell to element
            rValues = [xi*(r1 - r0)/2 + (r1 + r0)/2 for xi in coordinates]
            sValues = [et*(s1 - s0)/2 + (s1 + s0)/2 for et in coordinates]
            
            # Evaluate Lagrange polynomials in both directions
            shapesRDiff0 = [np.array([shape(r) for shape in shapesDiff0]) for r in rValues]
            shapesRDiff1 = [np.array([shape(r) for shape in shapesDiff1]) for r in rValues]
            shapesSDiff0 = [np.array([shape(s) for shape in shapesDiff0]) for s in sValues]
            shapesSDiff1 = [np.array([shape(s) for shape in shapesDiff1]) for s in sValues]
            
            for igauss, (r, w0) in enumerate(zip(rValues, weights)):
                for jgauss, (s, w1) in enumerate(zip(sValues, weights)):
                
                    x, y = mapX(ielement, r), mapY(jelement, s)
                    weight = 1/16 * (r1 - r0) * (s1 - s0) * dx * dy * w0 * w1
                    
                    # Compute tensor product and derivatives w.r.t. x and y
                    N    = np.outer( shapesRDiff0[igauss], shapesSDiff0[jgauss] ).ravel( )
                    dNdx = np.outer( shapesRDiff1[igauss] * 2 / dx, shapesSDiff0[jgauss] ).ravel( )
                    dNdy = np.outer( shapesRDiff0[igauss], shapesSDiff1[jgauss] * 2 / dy ).ravel( )
                                    
                    # Add contributions
                    Ke += ( np.outer( dNdx, dNdx ) + np.outer( dNdy, dNdy ) ) * E(x, y) * weight
                    Fe += N * fx(x, y) * weight
                    
        elementSlice = createElementSlice(ielement, jelement)

        # Repeat locationMaps as colums/rows linearize the result to obtain matrix entry coordinates
        row[elementSlice] = np.broadcast_to(locationMap, (ndofelement, ndofelement)).T.ravel( )
        col[elementSlice] = np.broadcast_to(locationMap, (ndofelement, ndofelement)).ravel( )
        
        # Set data
        Kdata[elementSlice] = Ke.ravel( )
        
        # Add to spatial rhs
        Fx[locationMap] += Fe
        
# Create coordinate matrix and convert to compressed sparse column format
M = scipy.sparse.coo_matrix((Mdata, (row, col)), shape=(ndofglobal, ndofglobal)).tocsc( )
K = scipy.sparse.coo_matrix((Kdata, (row, col)), shape=(ndofglobal, ndofglobal)).tocsc( )

print("Average bandwidth before filtering: " + "%.2f" % (M.nnz / M.shape[0]))
M.data[np.abs(M.data) < 1e-16 * scipy.sparse.linalg.norm(M)] = 0.0
M.eliminate_zeros()
print("Average bandwidth after filtering: " + "%.2f" % (M.nnz / M.shape[0]))

print( "Time integration (" + str( nsteps ) + " time steps) ... ", flush=True )

# Sparse LU factorization and time integration
factorized = scipy.sparse.linalg.splu( M )

u = np.zeros((nsteps + 1, ndofglobal))

for i in range( 2, nsteps + 1 ):
    u[i] = factorized.solve( M * ( 2 * u[i - 1] - u[i - 2] ) + dt**2 * ( Fx * ft( i * dt ) - K * u[i - 1] ) )

print( "Plotting ... ", flush=True )

# Plot animation (at Lagrange interpolation points)
x = [mapX(ielement, r) for ielement in range(nelements[0]) for r in lagrangeCoords[:-1]]
y = [mapY(jelement, s) for jelement in range(nelements[1]) for s in lagrangeCoords[:-1]]

x += [mapX(nelements[0] - 1, lagrangeCoords[-1])]
y += [mapY(nelements[1] - 1, lagrangeCoords[-1])]

X, Y = np.meshgrid(x, y, indexing='ij')
                   
Xe, Ye = np.meshgrid(np.linspace(0, lengths[0], nelements[0] + 1),
                     np.linspace(0, lengths[1], nelements[1] + 1), indexing='ij')
               
fig, ax = plt.subplots()
ax.set_aspect('equal', adjustable='box')

def animate(i):
    ax.clear()
    Z = np.reshape( u[i], (ndofdirection[0], ndofdirection[1]) )
    ax.contourf(X, Y, Z, levels=np.linspace(-1.0, 1.0, 24), cmap='seismic', extend='both')
    
    plt.plot(Xe, Ye, 'black')
    plt.plot(Ye, Xe, 'black')
    ax.add_artist(plt.Circle(center, radius, fill=False))
        
anim = animation.FuncAnimation(fig, animate, range(0, nsteps, 32), interval=1)

plt.show()
