import numpy
import scipy.interpolate
import matplotlib.pyplot as plt

nelements = 5
degree = 4
continuity = [2] * (nelements - 1)
#continuity = [1, 1]

internal = [i + 1.0 for i in range(nelements - 1) for _ in range(degree - continuity[i])]
allknots = (degree + 1) * [0.0] + internal + (degree + 1) * [nelements]

npoints = len(allknots)

x = numpy.linspace(0, nelements, 200)

for xi in range(nelements + 1):
    plt.plot([xi, xi], [0, 1], '--', color='black')
for ishape in range(len(allknots) - degree - 1):
    bspline = scipy.interpolate.BSpline(allknots, numpy.eye(npoints)[ishape], degree)
    plt.plot(x, bspline(x))
plt.show()