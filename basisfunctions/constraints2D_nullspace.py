import numpy, functools
import scipy.sparse
import scipy.interpolate
import matplotlib.pyplot as plt

import spspaces

degrees = [2, 2]
continuity = [[0, 1], [0, 1]]

# ------------- basis functions ------------
axisSlice = lambda p, axis : (numpy.newaxis, ) * axis + (slice(None), ) + (numpy.newaxis, ) * (len(p) - axis - 1)
construct = lambda p, axis : numpy.broadcast_to(numpy.eye(p[axis] + 1)[axisSlice(p, axis)], numpy.add(p + [p[axis]], 1))

coefficients = [numpy.array(construct(degrees, axis)) for axis, _ in enumerate(degrees)]

bernsteinFunction = lambda c, diff, t : scipy.interpolate.BPoly([[ci] for ci in c], [-1, 1]).derivative(diff)(t)[()]              
bernsteinProduct = lambda cList, diffs, coords : numpy.product([bernsteinFunction(c, d, t) for c, d, t in zip(cList, diffs, coords)], axis=0)
bernsteinBasis = lambda indices, diffs, coords : bernsteinProduct([ci[indices] for ci in coefficients], diffs, coords)

ndof = numpy.product(numpy.add(degrees, 1))

# --------------- constraints --------------
def evaluate(ishape, jshape, diffs, coords):
    entries = numpy.zeros([p + 1 for p in degrees] * 2)
    for indices in numpy.ndindex(*numpy.add(degrees, 1)):
        entries[(ishape, jshape) + indices] = bernsteinBasis(indices, diffs, coords)
    return entries.ravel()
    
def constrain(eval, value):
    if(numpy.linalg.norm(eval) < 1e-10):
        assert(value == 0.0)
        return
    C.append(eval)
    F.append(value)
    
C, F = [], []

for axis, (p, (c0, c1)) in enumerate(zip(degrees[0:2], continuity[0:2])):
    order = lambda v0, v1 : (v0, v1) if axis == 0 else (v1, v0)
    for jshape in range(degrees[1 - axis] + 1):
        for y in numpy.linspace(-1, 1, degrees[1 - axis] + 1):
            # First shapes on each element zero on right side
            for ishape in range(p - c1):
                for diff in range(p - ishape):
                    constrain(evaluate(*order(ishape, jshape), order(diff, 0), order(1, y)), 0)
                    
            # Last shapes on each element zero on left side 
            for ishape in range(c0 + 1, p + 1):
                for diff in range(ishape):
                    constrain(evaluate(*order(ishape, jshape), order(diff, 0), order(-1, y)), 0)

# Partition of unity
for x in numpy.linspace(-1, 1, degrees[0] + 1):
    for y in numpy.linspace(-1, 1, degrees[1] + 1):
        evals = []
        for i in range(degrees[0] + 1):
            for j in range(degrees[1] + 1):
                evals.append(evaluate(i, j, (0, 0), (x, y)))
        constrain(functools.reduce(numpy.add, evals), 1) 
  
# ------------- solve and plot -------------
 
#for ci in C:
#    line = f''
#    for c in ci:
#        line += f'{c:g} '
#    print(line)

C = numpy.array(C)
N = spspaces.nullspace(C).T.reshape([-1] + [p + 1 for p in degrees] * 2)

#N = scipy.linalg.null_space(C).T.reshape([-1] + [p + 1 for p in degrees] * 2)
#print(numpy.linalg.norm(numpy.dot(C, N[0].ravel())))

print(f'C.shape = {C.shape}')
print(f'N.shape = {N.shape}')

basis = [numpy.copy(c) for c in coefficients]

for ibasis, null in enumerate(N):
    
    # Update coefficients
    for i0 in range(degrees[0] + 1):
        for j0 in range(degrees[1] + 1):
            coefficients[0][i0, j0] *= 0
            coefficients[1][i0, j0] *= 0
            for i1 in range(degrees[0] + 1):
                for j1 in range(degrees[1] + 1):
                    factor0 = numpy.sqrt(abs(null[i0, j0, i1, j1]))
                    factor1 = numpy.sign(null[i0, j0, i1, j1]) * factor0
                    coefficients[0][i0, j0] += factor0 * basis[0][i1, j1]
                    coefficients[1][i0, j0] += factor1 * basis[1][i1, j1]
                    
    # Plot basis function grid
    factor = min(3, numpy.min(numpy.array([20, 12]) / numpy.add(degrees, 1)))
    fig, axes = plt.subplots(ncols=degrees[0] + 1, nrows=degrees[1] + 1, figsize = factor*numpy.add(degrees, 1))
    fig.suptitle(f'Null space basis function: {ibasis + 1} / {N.shape[0]}')
    
    X, Y = numpy.meshgrid(numpy.linspace(-1, 1, 20), numpy.linspace(-1, 1, 20), indexing='ij')
    
    for i in range(degrees[0] + 1):
        for j in range(degrees[1] + 1):
            axes[::-1].T[i, j].contourf(X, Y, bernsteinBasis((i, j), (0, 0), (X, Y)), vmin=-0.8, vmax=0.8, cmap='seismic')
    
    plt.tight_layout()
    plt.show()
    