import numpy, functools
import scipy.sparse
import scipy.sparse.linalg
import matplotlib.pyplot as plt

# nelements = 8
# degrees = [4] * nelements
# continuity = [3] * (nelements - 1)

degrees = [2, 2, 2]
continuity = [1, 1]

# Prepare
offsets = numpy.cumsum([0] + [(p + 1)**2 for p in degrees])
nelements = len(degrees)

locationMaps = [list(range(degrees[0] + 1))]

assert(len(degrees) == len(continuity) + 1)

for p, c in zip(degrees[1:], continuity):
    assert(c <= p)
    overlap, newdof = locationMaps[-1][-c-1:] if c >= 0 else [], locationMaps[-1][-1] + 1
    locationMaps.append(overlap + list(range(newdof, newdof + p - c)))

continuity = [-1] + continuity + [-1]

ndof = numpy.max([numpy.max(map) for map in locationMaps]) + 1

monomial = lambda power, diff, t : numpy.product([power - j for j in range(diff)])*t**(power - diff)
P = lambda ielement, diff, t: [0] * diff + [monomial(i, diff, t) for i in range(diff, degrees[ielement] + 1)]

# Constraints
def evaluate(ielement, ishape, diff, coord):
    entries = numpy.zeros((offsets[-1], ))
    offset = offsets[ielement] + (degrees[ielement] + 1) * ishape
    entries[offset:offset + degrees[ielement] + 1] = P(ielement, diff, coord)
    return entries
    
def constrain(eval, value):
    C.append(eval)
    F.append(value)

C, F = [], []

for ielement in range(nelements):
    p, c0, c1 = degrees[ielement], continuity[ielement], continuity[ielement + 1]
    
    # First shapes on each element zero on right side
    for ishape in range(p - c1):
        for diff in range(p - ishape):
            constrain(evaluate(ielement, ishape=ishape, diff=diff, coord=1), 0)
            
    # Last shapes on each element zero on left side 
    for ishape in range(c0 + 1, p + 1):
        for diff in range(ishape):
            constrain(evaluate(ielement, ishape=ishape, diff=diff, coord=0), 0)

    # Partition of unity
    for x in numpy.linspace(0, 1, p - c1 + 1)[:-1]:
        evals = [evaluate(ielement, ishape=ishape, diff=0, coord=x - ielement) for ishape in range(p + 1)]
        constrain(functools.reduce(numpy.add, evals), 1) 
  
# Continuity
for ielement1, ielement2 in zip(range(0, nelements - 1), range(1, nelements)):
    for ishape2 in range(continuity[ielement1 + 1] + 1):
        ishape1 = ishape2 + degrees[ielement1] - continuity[ielement1 + 1]
        for diff in range(continuity[ielement1 + 1] + 1):
            constrain(evaluate(ielement1, ishape=ishape1, diff=diff, coord=1) - 
                      evaluate(ielement2, ishape=ishape2, diff=diff, coord=0), 0)
     
print(f'number of dofs: {ndof}')
print(f'number of constraints: {len(C)}', flush=True)
print(f'number of coefficients: {offsets[-1]}', flush=True)
print(f'rank = {numpy.linalg.matrix_rank(C)}', flush=True)
print(f'condition number = {numpy.linalg.cond(C)}', flush=True)
print(f'sparsity = {scipy.sparse.csr_matrix(C).nnz/numpy.array(C).size:%}', flush=True)

C = numpy.array(C)
D = numpy.linalg.solve(C, F)
#D = numpy.dot(numpy.linalg.pinv(C), numpy.array(F))
#D = numpy.array(scipy.linalg.null_space(C))[:, 0]

#def callback(current):
#    print(f'iteration {callback.nit}: |Ax-b| = {numpy.linalg.norm(S*current - F):.5g}', flush=True)
#    callback.nit += 1
#callback.nit = 0
#
#S = scipy.sparse.csr_matrix(C)
#D, info = scipy.sparse.linalg.bicgstab(S, F, callback=callback)

# ======= postprocess =======
res = 100

t = numpy.linspace(0, 1, res)
x = numpy.concatenate([t + ielement for ielement in range(nelements)])

E = numpy.zeros((ndof, nelements*res))

for ielement in range(nelements):
    for ishape in range(degrees[ielement] + 1):
        offset = offsets[ielement] + (degrees[ielement] + 1)*ishape
        for factor, eval in zip(D[offset:offset + degrees[ielement] + 1], P(ielement, 0, t)):
            E[locationMaps[ielement][ishape], ielement*res:(ielement + 1)*res] += factor * eval

for i in range(ndof):
    plt.plot(x, E[i])
plt.show()