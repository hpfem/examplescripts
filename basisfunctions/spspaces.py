import numpy
import scipy.sparse
import scipy.linalg

#https://www.mathworks.com/matlabcentral/fileexchange/11120-null-space-of-a-sparse-matrix

#  PURPOSE: calculates the following decomposition
#             
#       A = L |Ubar  0 | Q
#             |0     0 |
#
#       where Ubar is a square invertible matrix
#       and matrices L, Q are invertible.
#
# ---------------------------------------------------
#  USAGE: [L,U,Q] = luq(A,do_pivot,tol)
#  INPUT: 
#         A             a sparse matrix
#         do_pivot      = 1 with column pivoting
#                       = 0 without column pivoting
#         tol           uses the tolerance tol in separating zero and
#                       nonzero values
#
#   OUTPUT:
#         L,U,Q          matrices
#
#   COMMENTS:
#         based on lu decomposition
#
# Copyright  (c) Pawel Kowal (2006)
# All rights reserved
# LREM_SOLVE toolbox is available free for noncommercial academic use only.
# pkowal3@sgh.waw.pl

def luq(A, do_pivot, tol):
    A = numpy.array(A)
    n, m = numpy.shape(A)
    
    # Special cases
    if A.shape[0] == 0:
        L = numpy.eye(n)
        U = A
        Q = numpy.eye(m)
        return L, U, Q
    
    if A.shape[1] == 0:
        L = numpy.eye(n)
        U = A    
        Q = numpy.eye(m)
        return L, U, Q
        
    # LU decomposition
    
    #if do_pivot:
    #    [P,L,U,Q]           = lu(A);   
    #    Q                   = Q';
    #else

    P, L, U = scipy.linalg.lu(A)
    Q = numpy.eye(m)

    p = A.shape[0] - L.shape[1]
    LL = numpy.concatenate([numpy.zeros((n-p,p)), numpy.eye(p)], axis=0)
    L = numpy.concatenate([numpy.dot(P.T, L), P[n-p:n,:].T], axis=1)
    U = numpy.concatenate([U,numpy.zeros((p,m))], axis=0)
    
    # FINDS ROWS WITH ZERO AND NONZERO ELEMENTS ON THE DIAGONAL
    if U.shape[0] == 1 or U.shape[1] == 1:
        S = U[0, 0]
    else:
        S = numpy.diag(U)
    I = numpy.where(numpy.abs(S) > tol)[0]
    Jl = numpy.delete(numpy.arange(n), I)
    Jq = numpy.delete(numpy.arange(m), I)
    Ubar1 = U[numpy.ix_(I,I)]
    Ubar2 = U[numpy.ix_(Jl,Jq)]
    Qbar1 = Q[I,:]
    Lbar1 = L[:,I]
    
    # ELININATES NONZEZO ELEMENTS BELOW AND ON THE RIGHT OF THE
    # INVERTIBLE BLOCK OF THE MATRIX U
    # UPDATES MATRICES L, Q
    if I.size > 0:
        Utmp                = U[numpy.ix_(I,Jq)]
        X                   = numpy.linalg.solve(Ubar1.T, U[numpy.ix_(Jl,I)].T);
        Ubar2               = Ubar2 - numpy.dot(X.T, Utmp)
        Lbar1               = Lbar1 + numpy.dot(L[:,Jl], X.T)
        X                   = numpy.linalg.solve(Ubar1, Utmp)
        Qbar1               = Qbar1 + numpy.dot(X, Q[Jq,:])
        Utmp                = []
        X                   = []
    
    # FINDS ROWS AND COLUMNS WITH ONLY ZERO ELEMENTS
    I2                      = numpy.where(numpy.max(numpy.abs(Ubar2),axis=1, initial=0) > tol)[0]
    I5                      = numpy.where(numpy.max(numpy.abs(Ubar2),axis=0, initial=0) > tol)[0]
    I3                      = Jl[I2]
    I4                      = Jq[I5]
    Jq = numpy.delete(Jq, I5)
    Jl = numpy.delete(Jl, I2)
    U                       = [];
    
    # FINDS A PART OF THE MATRIX U WHICH IS NOT IN THE REQIRED FORM
    A                       = Ubar2[numpy.ix_(I2,I5)]
    
    # PERFORMS LUQ DECOMPOSITION OF THE MATRIX A
    L1,U1,Q1                = luq(A, do_pivot, tol)
    
    # UPDATES MATRICES L, U, Q
    Lbar2                   = numpy.dot(L[:,I3], L1)
    Qbar2                   = numpy.dot(Q1, Q[I4,:])
    L                       = numpy.concatenate([Lbar1, Lbar2, L[:,Jl]], axis=1)
    Q                       = numpy.concatenate([Qbar1, Qbar2, Q[Jq,:]], axis=0)
    n1                      = len(I)
    n2                      = len(I3)
    m2                      = len(I4)
    U                       = numpy.block([[Ubar1, numpy.zeros((n1,m-n1))],
                                           [numpy.zeros((n2,n1)), U1, numpy.zeros((n2,m-n1-m2))],
                                           [numpy.zeros((n-n1-n2,m))]])

    return L, U, Q


#  PURPOSE: finds left and right null and range space of a sparse matrix A
#
# ---------------------------------------------------
#  USAGE: [SpLeft, SpRight] = spspaces(A,opt,tol)
#
#  INPUT: 
#       A                           a sparse matrix
#       opt                         spaces to calculate
#                                   = 1: left null and range space
#                                   = 2: right null and range space
#                                   = 3: both left and right spaces
#       tol                         uses the tolerance tol when calculating
#                                   null subspaces (optional)
#
#   OUTPUT:
#       SpLeft                      1x4 cell. SpLeft = {} if opt =2.
#           SpLeft{1}               an invertible matrix Q
#           SpLeft{2}               indices, I, of rows of the matrix Q that
#                                   span the left range of the matrix A
#           SpLeft{3}               indices, J, of rows of the matrix Q that
#                                   span the left null space of the matrix A
#                                   Q(J,:)A = 0
#           SpLeft{4}               inverse of the matrix Q
#       SpRight                     1x4 cell. SpRight = {} if opt =1.
#           SpLeft{1}               an invertible matrix Q
#           SpLeft{2}               indices, I, of rows of the matrix Q that
#                                   span the right range of the matrix A
#           SpLeft{3}               indices, J, of rows of the matrix Q that
#                                   span the right null space of the matrix A
#                                   AQ(:,J) = 0
#           SpLeft{4}               inverse of the matrix Q
#
#   COMMENTS:
#       uses luq routine, that finds matrices L, U, Q such that
#
#           A = L | U 0 | Q
#                 | 0 0 |
#       
#       where L, Q, U are invertible matrices, U is upper triangular. This
#       decomposition is calculated using lu decomposition.
#
#       This routine is fast, but can deliver inaccurate null and range
#       spaces if zero and nonzero singular values of the matrix A are not
#       well separated.
#
#   WARNING:
#       right null and rang space may be very inaccurate
#
# Copyright  (c) Pawel Kowal (2006)
# All rights reserved
# LREM_SOLVE toolbox is available free for noncommercial academic use only.
# pkowal3@sgh.waw.pl

#function [SpLeft, SpRight] = spspaces(A,opt,tol)

def spspaces(A,opt,tolerance=None):
    A = numpy.array(A)
    
    tol = tolerance
    if tolerance is None:
        tol = numpy.max([numpy.max(A.shape) * numpy.linalg.norm(A, ord=1) * 
            numpy.finfo(float).eps, 100*numpy.finfo(float).eps])
        
    if opt == 1:
            calc_left       = 1
            calc_right      = 0
    elif opt == 2:
            calc_left       = 0
            calc_right      = 1
    elif opt == 3:
            calc_left       = 1
            calc_right      = 1

    L,U,Q                   = luq(A,0,tol)
    
    if calc_left:
        if L.size > 0:
            LL              = numpy.linalg.inv(L)
        else:
            LL              = L

        S                   = numpy.max(numpy.abs(U), axis=1)
        I                   = numpy.where(S > tol)[0]
        
        if S.size > 0:
            J               = numpy.where(S <= tol)[0]
        else:
            J               = numpy.arange(S.shape[0])
   
        SpLeft              = [LL,I,J,L]
    else:
        SpLeft              = []

    if calc_right:
        if Q.size > 0:
            QQ              = numpy.linalg.inv(Q)
        else:
            QQ              = Q;
 
        S                   = numpy.max(numpy.abs(U), axis=0)
        I                   = numpy.where(S > tol)[0]
        
        if S.size > 0:
            J               = numpy.where(S <= tol )[0]
        else:
            J               = numpy.arange(S.shape[1])[numpy.newaxis, :]

        SpRight             = [QQ,I,J,Q]
    else:
        SpRight             = []

    
    return SpLeft, SpRight

def nullspace(M):
    QQ,I,J,Q = spspaces(M, 2)[1]
    return QQ[:, J]
    
#A = numpy.random.random((4, 6))
#SpLeft, SpRight = spspaces(A, 1)
