import numpy, functools
import scipy.sparse
import scipy.sparse.linalg
import scipy.interpolate
import matplotlib.pyplot as plt

#nelements = 4
#degrees = [4] * nelements
#continuity = [3] * (nelements - 1)

degrees =  [3, 2, 4, 3, 1]
continuity = [1, 0, 2, 0]

#degrees =  [5, 4, 3, 2, 1]
#continuity = [3, 2, 1, 0]

# ============== prepare ==============

assert(len(degrees) == len(continuity) + 1)
assert(numpy.all([c < p for c, p in zip(continuity, degrees)]))

nelements = len(degrees)
coefficients = [numpy.eye(p + 1) for p in degrees]

bernsteinFunction = lambda c, diff, t : scipy.interpolate.BPoly([[ci] for ci in c], [-1, 1]).derivative(diff)(t)[()]
bernsteinBasis = lambda ielement, diff, t : [bernsteinFunction(c, diff, t) for c in coefficients[ielement]]

def leftNullBasis(p, c):
    basis, zeros = lambda i : numpy.eye(p + 1)[i], [numpy.zeros(p + 1)]
    
    return [numpy.reshape(i * zeros + [basis(j), -basis(j)] + (p - 1 - i) * zeros, 
        [p + 1, p + 1]) for i in range(c) for j in range(i + 1)]

nullspaces = [leftNullBasis( p, c0 ) + [v[::-1, ::-1] for v in leftNullBasis( p, c1)]
              for p, c0, c1 in zip(degrees, [-1] + continuity, continuity + [-1])]

offsets = numpy.cumsum([0] + [len(basis) for basis in nullspaces])
 
# ============== assemble ==============

C = numpy.zeros((offsets[-1], offsets[-1]))
F = numpy.zeros(offsets[-1])

print(f'number of basis dofs: {offsets[-1]}')

row = 0
for c, ielement0, ielement1 in zip(continuity, range(nelements - 1), range(1, nelements)):
    for ishape in range(c)[::-1]:
        for diff in range(c + 1):
            for i, null in enumerate(nullspaces[ielement0]):
                C[row, offsets[ielement0] + i] += bernsteinFunction(null[-1-c + ishape], diff, 1.0)
                
            for i, null in enumerate(nullspaces[ielement1]):
                C[row, offsets[ielement1] + i] -= bernsteinFunction(null[ishape], diff, -1.0)
                
            F[row] = -bernsteinBasis(ielement0, diff, 1.0)[-1-c + ishape] + \
                      bernsteinBasis(ielement1, diff, -1.0)[ishape]    
            row += 1
   
# ============ solve and update ============
D = numpy.linalg.solve(C, F)

for ielement in range(nelements):
    for i, null in enumerate(nullspaces[ielement]):
        coefficients[ielement] += D[offsets[ielement] + i] * null
  
# ============== postprocess ==============
locationMaps = [list(range(degrees[0] + 1))]

for p, c in zip(degrees[1:], continuity):
    overlap, newdof = locationMaps[-1][-c-1:] if c >= 0 else [], locationMaps[-1][-1] + 1
    locationMaps.append(overlap + list(range(newdof, newdof + p - c)))

res, ndof = 100, numpy.max([numpy.max(map) for map in locationMaps]) + 1

E, x = numpy.zeros((ndof, nelements*res)), numpy.zeros(nelements*res)
for ielement in range(nelements):
    for i, ti in enumerate(numpy.linspace(-1, 1, res)):
        x[ielement*res + i] = ti / 2 + 1 / 2 + ielement
        E[locationMaps[ielement], ielement*res + i] = bernsteinBasis(ielement, 0, ti)
    plt.plot([ielement + 1, ielement + 1], [0, 1], '--', color='black')
plt.plot([0, 0], [0, 1], '--', color='black')

for i in range(ndof):
    plt.plot(x, E[i])
plt.show()

# ============== print matrix ==============
for iblock0, iblock1 in zip(offsets[:-1], offsets[1:]):
    for row in C[iblock0:iblock1, :]:
        str = f''
        for jblock0, jblock1 in zip(offsets[:-1], offsets[1:]):
            for j, entry in enumerate(row[jblock0:jblock1]):
                str += f'{entry}  '
            str += f'    '
        print(str)
    print('')
   
