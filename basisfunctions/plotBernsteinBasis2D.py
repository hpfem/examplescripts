import numpy
import scipy.interpolate
import matplotlib.pyplot as plt

degrees = [3, 2]
diff = [0, 0]

# Create Bernstein coefficients on [p + 1, q + 1] grid. Like this for p = (2, 3):
# [[[[1, 0, 0], [0, 1, 0], [0, 0, 1]],    [[[1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]],
#   [[1, 0, 0], [0, 1, 0], [0, 0, 1]],     [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]],
#   [[1, 0, 0], [0, 1, 0], [0, 0, 1]],     [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0]],
#   [[1, 0, 0], [0, 1, 0], [0, 0, 1]]],    [[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]]]
axisSlice = lambda p, axis : (numpy.newaxis, ) * axis + (slice(None), ) + (numpy.newaxis, ) * (len(p) - axis - 1)
construct = lambda p, axis : numpy.broadcast_to(numpy.eye(p[axis] + 1)[axisSlice(p, axis)], numpy.add(p + [p[axis]], 1))

coefficients = [construct(degrees, axis) for axis, _ in enumerate(degrees)]

# Evaluate single function, product of two functions given coefficients, and given i/j indices
bernsteinFunction = lambda c, diff, t : scipy.interpolate.BPoly([[ci] for ci in c], [-1, 1]).derivative(diff)(t)[()]              
bernsteinProduct = lambda cList, diffs, coords : numpy.product([bernsteinFunction(c, d, t) for c, d, t in zip(cList, diffs, coords)], axis=0)
bernsteinBasis = lambda indices, diffs, coords : bernsteinProduct([ci[indices] for ci in coefficients], diffs, coords)

# Evaluate and visualize
factor = min(3, numpy.min(numpy.array([20, 12]) / numpy.add(degrees, 1)))
fig, axes = plt.subplots(ncols=degrees[0] + 1, nrows=degrees[1] + 1, figsize = factor*numpy.add(degrees, 1))

X, Y = numpy.meshgrid(numpy.linspace(-1, 1, 20), numpy.linspace(-1, 1, 20), indexing='ij')

for i in range(degrees[0] + 1):
    for j in range(degrees[1] + 1):
        axes[::-1].T[i, j].contourf(X, Y, bernsteinBasis((i, j), diff, (X, Y)))

plt.tight_layout()
plt.show()