import numpy, functools
import scipy.sparse
import scipy.sparse.linalg
import matplotlib.pyplot as plt
import spspaces

p = 3
c0, c1 = -1, 2

monomial = lambda power, diff, t : numpy.product([power - j for j in range(diff)])*t**(power - diff)
basis = lambda diff, t: [0] * diff + [monomial(i, diff, t) for i in range(diff, p + 1)]

# Constraints
def evaluate(ishape, diff, coord):
    entries = numpy.zeros((p + 1)**2)
    entries[ishape * (p + 1):(ishape + 1) * (p + 1)] = basis(diff, coord)
    return entries
    
def constrain(eval, value):
    C.append(eval)
    F.append(value)

C, F, rowOffsets = [], [], [0]

print('First shapes on each element zero on right side')
for ishape in range(p - c1):
    for diff in range(p - ishape):
        dd = '\''*diff
        print(f'N{ishape}{dd}(1) = 0')
        constrain(evaluate(ishape=ishape, diff=diff, coord=1), 0)
        
        
print('Last shapes on each element zero on left side')
for ishape in range(c0 + 1, p + 1):
    for diff in range(ishape):
        dd = '\''*diff
        print(f'N{ishape}{dd}(0) = 0')
        constrain(evaluate(ishape=ishape, diff=diff, coord=0), 0)

# Partition of unity
poiPoints = numpy.linspace(0, 1, p + 3)
print(f'partition of unity at {poiPoints}')
for x in numpy.linspace(0, 1, p + 5):
    evals = [evaluate(ishape=ishape, diff=0, coord=x) for ishape in range(p + 1)]
    constrain(functools.reduce(numpy.add, evals), 1) 
    
C = numpy.array(C)
F = numpy.array(F)

print('===================================================')

bernstein = [[],
             [1, -1, 
              0,  1],
             [1, -2,  1,
              0,  2, -2,
              0,  0,  1],
             [1, -3,  3, -1,
              0,  3, -6,  3,
              0,  0,  3, -3,
              0,  0,  0,  1],
             [1,  -4,   6,  -4,  1,
              0,   4, -12,  12, -4,
              0,   0,   6, -12,  6,
              0,   0,   0,   4, -4,
              0,   0,   0,   0,  1]]

#pB30 = [1, -3,  3, -1]
#pB31 = [0,  3, -6,  3]
#pB33 = [0, 0, 0, 1]
#pB32 = [0, 0, 3, -3]
#
#___3 = [0, 0, 0, 0]
#
#_B30, _B31, _B32, _B33 = [[-v for v in V] for V in (pB30, pB31, pB32, pB33)]
#
#null = [[],
#        [ ___3 + ___3 + pB33 + _B33,
#          pB30 + _B30 + ___3 + ___3],
#        [ ___3 + ___3 + pB33 + _B33,
#          ___3 + pB33 + ___3 + _B33,
#          ___3 + pB32 + _B32 + ___3,
#          pB30 + _B30 + ___3 + ___3,
#          pB30 + ___3 + _B30 + ___3,
#          ___3 + pB31 + _B31 + ___3]]
    
N = spspaces.nullspace(C).T
B = numpy.dot(numpy.linalg.pinv(C), F)

print(f'C.shape = {numpy.shape(C)}')
print(f'basis: {numpy.linalg.norm(numpy.dot(C, bernstein[p])-F)}')
print(f'rank of C: {numpy.linalg.matrix_rank(C)} / {C.shape[1]}, nullspace rank: {N.shape}')


#print(B)
#print(scipy.linalg.null_space(Ci).T[0])
#print(numpy.linalg.norm(numpy.dot(C, bernstein[3] + scipy.linalg.null_space(C).T[0]) - F))

D = N[0]

#scipy.linalg.null_space(C).T[0]# / 0.175938706

# ======= postprocess =======
res = 100
t = numpy.linspace(0, 1, res)
E = numpy.zeros((p + 1, res))

for ishape in range(p + 1):
    offset = (p + 1)*ishape
    for factor, eval in zip(D[(p + 1)*ishape:(p + 1)*(ishape + 1)], basis(0, t)):
        E[ishape] += factor * eval

for e in E:
    plt.plot(t, e)
plt.show()