import numpy, functools
import scipy.sparse
import scipy.sparse.linalg
import scipy.interpolate
import matplotlib.pyplot as plt

import mlhpc

ndim = 2

nelements = [2, 2]

P = [[2, 2]] * numpy.product(nelements)
       
#P = [[2, 2], [1, 1], [1, 1], [1, 1]]
       
# Construct continuity matrix
grid = mlhpc.makeRefinedGrid(nelements)
N = grid.neighbors
I = [(a, i, n[a, 1]) for i, n in enumerate(N) for a in range(N.shape[1]) if n[a, 1] != -1]
   
#continuity = [1 if i0 == 4 or i1 == 4 else 1 for a, i0, i1 in I]
continuity = [1, 1, 1, 1]

continuityMatrix = numpy.full(N.shape, -1)

for ci, (a, i0, i1) in zip(continuity, I):
    continuityMatrix[i0][a, 1] = ci
    continuityMatrix[i1][a, 0] = ci
        
# Construct location matrices
locationMatrices = mlhpc.createMlhpcLocationMatrices(N, P, continuityMatrix)
#L = numpy.array([[[0, 1, 2], 
#                  [3, 4, 5], 
#                  [6, 7, 8]],
#                 [[1, 2, 9], 
#                  [4, 5, 10], 
#                  [7, 8, 11]],
#                 [[3, 4, 5], 
#                  [12, 13, 8], 
#                  [14, 15, 16]],
#                 [[4, 5, 10], 
#                  [7, 8, 11], 
#                  [16, 17, 18]]])

#locationMatrices = numpy.array([[[0, 1, 2], 
#                                [3, 4, 5], 
#                                [6, 7, 8]],
#                               [[1, 2, 9], 
#                                [4, 5, 10], 
#                                [7, 8, 11]],
#                               [[6, 4, 5], 
#                                [12, 7, 8], 
#                                [13, 14, 15]],
#                               [[4, 5, 10], 
#                                [7, 8, 11], 
#                                [15, 16, 17]]])

# ------------- basis functions ------------
axisSlice = lambda p, axis : (numpy.newaxis, ) * axis + (slice(None), ) + (numpy.newaxis, ) * (len(p) - axis - 1)
broadcast = lambda c, p, axis : numpy.broadcast_to(c[axisSlice(p, axis)], numpy.add(p + [p[axis]], 1)) * 1
standardBasis = lambda p, axis : broadcast(numpy.eye(p[axis] + 1), p, axis)

coefficients2 = [[standardBasis(p, axis) for axis, _ in enumerate(p)] for p in P]

bernsteinFunction = lambda c, diff, t : scipy.interpolate.BPoly([[ci] for ci in c], [-1, 1]).derivative(diff)(t)[()]              
bernsteinProduct = lambda cList, diffs, coords : numpy.product([bernsteinFunction(c, d, t) for c, d, t in zip(cList, diffs, coords)], axis=0)
bernsteinBasis = lambda ielement, indices, diffs, coords : bernsteinProduct([ci[indices] for ci in coefficients2[ielement]], diffs, coords)

# ------------- null basis ------------
def leftNullBasis1D(p, c):
    basis, zeros = lambda i : numpy.eye(p + 1)[i], [numpy.zeros(p + 1)]
    
    return [numpy.reshape(i * zeros + [basis(j), -basis(j)] + (p - 1 - i) * zeros, 
        [p + 1, p + 1]) for i in range(c) for j in range(i + 1)]

nullBasis1D = lambda p, c01 : leftNullBasis1D(p, c01[0]) + [v[::-1, ::-1] for v in leftNullBasis1D(p, c01[1])]

def constructNullbasis(p, c):
    null, axes = [[], []], []
    
    for n in nullBasis1D(p[0], c[0]):
        for i in range(p[1] + 1):
            a = numpy.zeros([pi + 1 for pi in p] + [p[0] + 1])
            b = numpy.zeros([pi + 1 for pi in p] + [p[1] + 1])
            a[:, i] = broadcast(n, p, 0)[:, i]
            b[:, i] = standardBasis(p, 1)[:, i] * numpy.sum(numpy.abs(n), axis=-1)[:, numpy.newaxis]
            null[0].append(a)
            null[1].append(b)
            axes.append(0)
            
    for n in nullBasis1D(p[1], c[1]):
        for i in range(p[0] + 1):
            a = numpy.zeros([pi + 1 for pi in p] + [p[0] + 1])
            b = numpy.zeros([pi + 1 for pi in p] + [p[1] + 1])
            a[i, :] = standardBasis(p, 0)[i, :] * numpy.sum(numpy.abs(n), axis=-1)[:, numpy.newaxis]
            b[i, :] = broadcast(n, p, 1)[i, :]
            null[0].append(a)
            null[1].append(b)
            axes.append(1)
            
    return [numpy.array(n) for n in null], axes
    
nullspaces = [constructNullbasis(p, c) for p, c in zip(P, continuityMatrix)]
nullspaces, axes = [n[0] for n in nullspaces], [n[1] for n in nullspaces] 
offsets = numpy.cumsum([0] + [len(nullspace[0]) for nullspace in nullspaces])


#C = numpy.zeros((offsets[-1], offsets[-1]))
#F = numpy.zeros(offsets[-1])

#row = 0
C, F = [], []

def sharedDofs(locationMap0, locationMap1):
    shared = []
    for ijk in numpy.ndindex(*locationMap0.shape):
        dofIndex = locationMap0[ijk]
        if dofIndex != -1:
            other = numpy.array(numpy.where(locationMap1 == dofIndex))
            if other.size:
                assert(other.shape == (2, 1))
                shared.append((dofIndex, ijk, tuple(other[:,0].tolist())))
    return shared

for ci, (a, ielement0, ielement1) in zip(continuity, I):   
    print(f'interface {ielement0} <--> {ielement1}')
    
    maxdegrees = numpy.max([P[ielement0], P[ielement1]], axis=0)

    for dofIndex, ijk0, ijk1 in sharedDofs(locationMatrices[ielement0], locationMatrices[ielement1]):
        for y in numpy.linspace(-1.0, 1.0, maxdegrees[1 - a] + 1):
            for diff in range(ci + 1):
                order = lambda v0, v1 : (v0, v1) if a == 0 else (v1, v0)
                coords0, coords1, diffs = order(1.0, y), order(-1.0, y), order(diff, 0)
                
                row = numpy.zeros(offsets[-1])
                for i, (nullX, nullY) in enumerate(zip(*nullspaces[ielement0])):
                    row[offsets[ielement0] + i] += bernsteinProduct([nullX[ijk0], nullY[ijk0]], diffs, coords0)
                for i, (nullX, nullY) in enumerate(zip(*nullspaces[ielement1])):
                    row[offsets[ielement1] + i] -= bernsteinProduct([nullX[ijk1], nullY[ijk1]], diffs, coords1)
                        
                value = bernsteinBasis(ielement1, ijk1, diffs, coords1) - \
                        bernsteinBasis(ielement0, ijk0, diffs, coords0)
                      
                if len(C) == 0:
                    C.append(row)
                    F.append(value)
                elif numpy.linalg.norm(row) > 1e-6:
                    i = numpy.where(numpy.abs(row) > 1e-6)[0][0]
                    if numpy.abs(C[-1][i]) <= 1e-6 or numpy.linalg.norm(row - row[i] / C[-1][i] * C[-1]) > 1e-6:
                        C.append(row)
                        F.append(value)
                        
                    
    #for ishape in range(ci)[::-1]:
    #    for diff in range(ci + 1):
    #        for i, null in enumerate(nullspaces[ielement0][a]):
    #            C[row, offsets[ielement0 * ndim + a] + i] += bernsteinFunction1(null[-1-ci + ishape], diff, 1.0)
    #        for i, null in enumerate(nullspaces[ielement1][a]):
    #            C[row, offsets[ielement1 * ndim + a] + i] -= bernsteinFunction1(null[ishape], diff, -1.0)
    #            
    #        F[row] = -bernsteinBasis1(ielement0, a, diff, 1.0)[-1-ci + ishape] + \
    #                  bernsteinBasis1(ielement1, a, diff, -1.0)[ishape]    
    #        row += 1

C = numpy.array(C)

#D = numpy.linalg.solve(C, F)
#print(f'solution = {D}')

print('C.shape = ', numpy.array(C).shape)
print('N.shape = ', scipy.linalg.null_space(numpy.transpose(C)).shape)
print(numpy.array(F).shape)


#[Q, R] = scipy.linalg.qr(C)
#D = scipy.linalg.solve_triangular(R[:offsets[-1]], Q[:, :offsets[-1]].T @ F)

D = scipy.linalg.solve(numpy.array(C).T @ C, numpy.array(C).T @ F)

print('|| C * D - D || = ', numpy.linalg.norm(numpy.array(C).T @ (C @ D) - numpy.array(C).T @ F))
print('|| C * D - D || = ', numpy.linalg.norm(C @ D - F))

#D = numpy.zeros(offsets[-1])
#D[0] = 1
#coefficients2 = [[c0 * 0, c1 * 0] for c0, c1 in coefficients2]

for ielement, nullspace in enumerate(nullspaces):
    for axis, null in enumerate(nullspace):
        for i, vector in enumerate(null):
            coefficients2[ielement][axis] += (axis == axes[ielement][i]) * D[offsets[ielement] + i] * vector

# Accumulates basis data and helps evaluating basis-related information
class Basis:
    def __init__(self, grid, maps):
        self.grid = grid
        self.dofmaps = maps
        self.ndof = numpy.max([numpy.max(Gi.ravel()) for Gi in maps if Gi.size]) + 1
        self.nelements = len(maps)
        
    def maxdegrees(self, cell):
        return self.dofmaps[cell].shape
            
    def locationMap(self, cell):
        return self.dofmaps[cell].ravel()
        
    def ndofelement(self, cell):
        return numpy.count_nonzero(self.masks[cell].ravel())
        
    def evaluate(self, cell, rstVectors, diff, mapDerivatives=True):
        ndim = self.grid.ndim 
        
        X, Y = numpy.meshgrid(*rstVectors, indexing='ij')
        
        shapes = numpy.zeros([p + 1 for p in P[cell]] + [numpy.product([len(v) for v in rstVectors])])
        for indices in numpy.ndindex(*[p + 1 for p in P[cell]]):
            shapes[indices] = bernsteinBasis(cell, indices, [diff] * 2, (X, Y)).ravel()
        shapes = [shapes.reshape([numpy.product(self.dofmaps[cell].shape), -1]).T]
        mapped = self.grid.mapBasisFunctions(cell, rstVectors, [], [0]*ndim) if mapDerivatives else None
        
        return [shapes, mapped] if mapDerivatives else shapes
        
basis = Basis(grid, locationMatrices)   
    
for i in range(basis.ndof):
    postprocessors = [mlhpc.makeBasisFunctionPostprocessor(i, name='Value')]
    mlhpc.postprocessVtu(basis, postprocessors, f'outputs/basisfunction_{i}.vtu', mlhpc.pplus(8))
    
    
print('location matrices:')
indices = numpy.arange(numpy.product(nelements)).reshape(nelements).T[::-1]

for row in indices:
    for ishape in range(numpy.max([locationMatrices[i].shape[1] for i in row])):
        str = f''
        for ielement in row:
            stri = f''
            if ishape <= P[ielement][1]:
                stri = f'{locationMatrices[ielement].reshape([pi + 1 for pi in P[ielement]]).T[::-1][ishape]}'
            str += stri + ' ' * (15 - len(stri))
        print(str)
    print('\n')
    
C, F = [], []
for ci, (a, i0, i1) in zip(continuity, I):
    pass
    
    