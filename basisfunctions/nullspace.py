import matplotlib.pyplot as plt
import scipy.linalg
import numpy
import sympy 
import mlhp

p = [2, 2]
c = [1]

C = []

ndof = numpy.sum([pi + 1 for pi in p])

#count = 0
#for i in range(len(p) - 1):
#    p0, p1, ci = p[i], p[i + 1], c[i]
#    for d in range(ci + 1):
#        Ci = numpy.zeros(ndof)
#        Ci[count:count + p0 + 1] = mlhp.integratedLegendre(1.0, p0, d)
#        Ci[count + p0 + 1:count + p0 + p1 + 2] = -1 * mlhp.integratedLegendre(-1.0, p1, d)
#        C.append( Ci )
#    count += p0 + 1
   
for d in range(numpy.max(c) + 1):
    count = 0
    for i in range(len(p) - 1):
        p0, p1, ci = p[i], p[i + 1], c[i]
        print("d = " + str(d) + ", i = " + str(i))
        if ci >= d:
            print("Add")
            Ci = numpy.zeros(ndof)
            Ci[count:count + p0 + 1] = mlhp.integratedLegendre(1.0, p0, d)
            Ci[count + p0 + 1:count + p0 + p1 + 2] = -1 * mlhp.integratedLegendre(-1.0, p1, d)
            C.append( Ci )
            
        count += p0 + 1
  
  
C = numpy.array(C)

#exit()

#B = [[1, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 1, 0, 0, 0, 0, 0, 0],
#     [0, 1, 0, 1, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 1, 0, 0, 0],
#     [0, 0, 0, 0, 1, 0, 1, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 1, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 1]]

def nullspace(C):
    R, P = sympy.Matrix(C).rref()
    N = numpy.zeros([R.shape[1], R.shape[1] - len(P)])
    
    Q = numpy.full(R.shape[1], True)
    Q[[pi for pi in P]] = False
    Q = numpy.where(Q)[0]
    
    for i, pi in enumerate(P):
        for j, qj in enumerate(Q):
            N[pi, j] = -R[i, qj]
    
    for i, qi in enumerate( Q ):
        N[qi, i] = 1.0
    
    return N


#N = scipy.linalg.null_space(C)
N = nullspace(C)

N = N.T

N = [[1,  0,  0.41, 0,   0,  0   ],
     [0,  1, -1.23, 1,   0,  0.41],
     [0,  1,  0.41, 1,   0, -1.23],
     [0,  0,  0,    0,   1,  0.41]]


N = [[1,   0.41, 0,   0   ,   0],
     [0,  -1.23, 1,   0.41,   0],
     [0,   0.41, 1,  -1.23,   0],
     [0,   0,    0,   0.41,   1]]
     
print(N)


#for j in range(len(P), R.shape[1]):
#    for i in range(0, R.shape[0]):
#        N[i, j - len(P)] = R[i,j]
    #N[j - len(P),j - len(P)] = 1
#for i in range(R.shape[1] - len(P)):
#    N[i + len(P), i + len(P)] = 1
 
#N[2, 2] = 1
#N[3, 3] = 1  
    
    
r = numpy.linspace(-1, 1, 50);

x = [r + 2 * i for i in range(len(p))]
x = numpy.concatenate(x, axis=0)


#exit()
for n in N:
    fi = []    
    count = 0
    for i, pi in enumerate(p):
        for ri in r:
            N = mlhp.integratedLegendre(ri, pi, 0)
            value = 0
            for Ni, ci in zip(N, n[count:count + pi + 1]):
                value += Ni * ci
            fi.append(value)
        count += pi + 1
    plt.plot(x, fi)
plt.show()
    
 