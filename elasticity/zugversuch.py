import numpy as np
import scipy
import math
import time

insertEntry = lambda V, i, v: V[:i] + (v, ) + V[i:]

def createCartesianGrid(nelements: tuple, 
                        lengths: tuple = None, 
                        origin: tuple = None):
    """Creates Nodes and Element connectivity for a Cartesian Grid"""
    lengths_ = [1.0 for _ in nelements] if lengths is None else lengths
    origin_ = [0.0 for _ in nelements] if origin is None else origin
    
    ndim = len(nelements)
    nvertices = np.add(nelements, 1)
    
    if np.prod(nelements) == 0:
        raise ValueError("Zero number of elements")
    
    if len(lengths_) != ndim or len(origin_) != ndim:
        raise ValueError("Inconsistent input dimensionality")
    
    # Node coordinates
    axes = np.mgrid[tuple(slice(n) for n in nvertices)]
    coordinates = [x / n * l + o for x, n, l, o in zip(axes, nelements, lengths_, origin_)]
    coordinates = np.reshape(coordinates, (ndim, -1)).transpose()
    
    # Element node connectivity
    nodeIds = np.arange(np.prod(nvertices)).reshape(nvertices)
    slices = [tuple(slice(i, i + n) for i, n in zip(ijk, nelements)) 
              for ijk in np.ndindex(*([2] * ndim))]
    connect = np.reshape([nodeIds[s] for s in slices], (2**ndim, -1)).T
    
    return coordinates, connect



def plainStrainMatrix(E, nu):
    tmp = E / ((1 + nu) * (1 - 2 * nu))
         
    return np.array([[tmp * (1 - nu), tmp * nu, 0],
                     [tmp * nu, tmp * (1 - nu), 0],
                     [0, 0, tmp / 2 * (1 - 2 * nu)]])
   
           
def linearIsotropicMatrix(E, nu):
    # Compute Lame parameters
    tmp1 = ( 1.0 - 2.0 * nu )
    tmp2 = E / ( ( 1.0 + nu ) * tmp1 )
    lmbd = nu * tmp2
    mu = 0.5 * tmp1 * tmp2
    diag = lmbd + 2.0 * mu
    
    return np.array([[diag, lmbd, lmbd,    0,    0,    0],
                     [lmbd, diag, lmbd,    0,    0,    0],
                     [lmbd, lmbd, diag,    0,    0,    0],
                     [   0,    0,    0,   mu,    0,    0],
                     [   0,    0,    0,    0,   mu,    0],
                     [   0,    0,    0,    0,    0,   mu]])


def defaultLinearElasticMaterial(ndim, E, nu):
    if ndim == 3: return linearIsotropicMatrix(E, nu)[np.newaxis, ...]
    if ndim == 2: return plainStrainMatrix(E, nu)[np.newaxis, ...]
    if ndim == 1: return np.array([[E]])
    raise ValueError("No linear elastic material for given dimension.")    
    
    
def referenceShapeFunctions(rst):
    ndim = len(rst)
    
    # Construct 1D shape functions    
    Ni = [np.array([(1 - ci) / 2, (1 + ci) / 2]) for ci in rst]
    dNi = [np.array([-1/2, 1/2]) for _ in rst]
   
    # Tensor product
    N = math.prod(np.ix_(*Ni)).ravel()
    dNdr = [[dNi[j] if j == i else Ni[j] for j in range(ndim)] for i in range(ndim)]
    dNdr = np.array([math.prod(np.ix_(*dNi)).ravel() for dNi in dNdr])
   
    return N, dNdr
   
    
# Voigt Notation: For each component (6 in 3D) the diff direction for each field component (3 in 3D)
# 1D: [[0]]
# 2D: [[0, None], [None, 1], [1, 0]]
# 3D: [[0, None, None], [None, 1, None], [None, None, 2], [1, 0, None], [2, None, 0], [None, 2, 1]]
#
# Which corresponds to the following (linearized symmetric deformation gradient, but engineering strain, ...right?):
# 1D: [du/dx]
# 2D: [du/dx, dv/dy, du/dy + dv/dx]
# 3D: [du/dx, dv/dy, dw/dz, du/dy + dv/dx, du/dz + dw/dx, dv/dz + dw/dy]
def strainOperator(dNdx):
    ndim = dNdx.shape[1]
    
    components = [[i if i == j else None for j in range(ndim)] for i in range(ndim)]
    components += [[j if k == i else (i if k == j else None) for k in range(ndim)] 
                    for i in range(ndim) for j in range(i)]
    
    # Construct B-operator using components to extract from dN
    B = np.array([[dNdx[:, diff] if diff is not None else 0*dNdx[:, axis] 
                   for axis, diff in enumerate(ci)] for ci in components])
    
    return np.concatenate(np.transpose(B, (1, 2, 0, 3)), axis=2)
    
    
def integrateBtCB(Ke, B, C, W):
    CW = C * np.expand_dims(W, (1, 2))
    tmp1 = np.zeros((B.shape[2], B.shape[2]))
    tmp2 = np.zeros((B.shape[2], B.shape[1]))
    
    for i in range(Ke.shape[0]):
        Ke[i] += np.matmul(np.matmul(B[i].T, CW[i], out=tmp2), B[i], out=tmp1)
        # Ke[e0 + i] += B[i].T @ C[0] @ B[i] * W[i]
    #Ke[e0:e1] += np.einsum("aki,akl,alj,a->aij", B, C, B, W, optimize='optimal')
    
    
def integrateLinearElasticity(nodes, connectivity, gaussorder, E, nu):
    (nelements, nnodeelement), (_, ndim) = connectivity.shape, nodes.shape
    ndofelement = ndim * nnodeelement
    
    gauss = [np.polynomial.legendre.leggauss(order) for order in gaussorder]
    Ke = np.zeros((nelements, ndofelement, ndofelement))
      
    for ijk in np.ndindex(*gaussorder):
        rst = tuple(xw[0][i] for i, xw in zip(ijk, gauss))
        weight = np.prod([xw[1][j] for j, xw in zip(ijk, gauss)])
        
        N, dNdr = referenceShapeFunctions(rst)
        
        J = np.einsum("jk, ikl", dNdr, nodes[connectivity])
        W = weight * np.linalg.det(J)
        dNdx = np.linalg.solve(J, dNdr[np.newaxis, ...])
        
        B = strainOperator(dNdx)
        C = defaultLinearElasticMaterial(ndim, E, nu)
        
        integrateBtCB(Ke, B, C, W)
    return Ke


def integrateLinearElasticity2(arguments):
    return integrateLinearElasticity(*arguments)


def assembleLinearElasticity(nodes, connectivity, E, nu, step=10000, pool=None):
    (nelements, nnodeelement), (nnodes, ndim) = connectivity.shape, nodes.shape
    ndof, ndofelement = ndim * nnodes, ndim * nnodeelement
    gaussorder = (2, ) * ndim
    
    # Assembly
    Ke = np.zeros((nelements, ndofelement, ndofelement))
     
    nsteps = nelements // step + 1
    chunks = np.linspace(0, nelements, nsteps + 1, dtype=int)
    
    t1 = time.time()
    if pool is not None:
        args = ((nodes, connectivity[chunks[i]:chunks[i + 1]], gaussorder, E, nu) for i in range(len(chunks) - 1))    
        for ichunk, Kei in enumerate(pool.map(integrateLinearElasticity2, args)):
            Ke[chunks[ichunk]:chunks[ichunk + 1]] += Kei
    else:
        for ichunk in range(len(chunks) - 1):
            Ke[chunks[ichunk]:chunks[ichunk + 1]] += integrateLinearElasticity(nodes, 
               connectivity[chunks[ichunk]:chunks[ichunk + 1]], gaussorder, E, nu)
            
    print(f"Assembly with {len(chunks) - 1} chunks took {time.time() - t1:.5g} seconds.")
        
    # Location maps and sparse matrix creation
    LM = np.concatenate([connectivity + i*nnodes for i in range(ndim)], axis=1)
    
    colIDs = np.repeat(LM, ndofelement, axis=0).ravel()
    rowIDs = np.repeat(LM, ndofelement, axis=1).ravel()
    
    K = scipy.sparse.coo_matrix((Ke.ravel(), (rowIDs, colIDs)), shape=(ndof, ndof)).tocsr()
    F = np.zeros(K.shape[0])
    
    return K, F, LM


def imposeDirichlet(K, F, dirichletIds, dirichletValues):
    F -= K[:, dirichletIds] @ dirichletValues
    F[dirichletIds] = dirichletValues
    
    diag = K.diagonal()
    diag[dirichletIds] = 1
    
    dirichletMask = np.zeros(K.shape[0], dtype=bool)
    dirichletMask[dirichletIds] = True
    
    for begin, end in zip(K.indptr[:-1][dirichletMask], K.indptr[1:][dirichletMask]):
        K.data[begin:end] = 0.0
    
    K.data[dirichletMask[K.indices]] = 0.0
    K.setdiag(diag)
    
    
def equivalentStress(stress):
    if stress.ndim != 2:
        raise ValueError(f"Invalid stress dimensions: shape = {stress.shape}.")
    
    match stress.shape[1]:
        case 1:
            return stress.T[0]
        
        case 3:
            S11, S22, S12 = stress.T
            
            return np.sqrt(S11**2 - S11 * S22 + S22**2 + 3 * S12**2 )
        
        case 6:
            S11, S22, S33, S12, S13, S23 = stress.T
            
            D1 = (S11 - S22) * (S11 - S22)
            D2 = (S22 - S33) * (S22 - S33)
            D3 = (S33 - S11) * (S33 - S11)
            
            S = S12 * S12 + S23 * S23 + S13 * S13

            return np.sqrt(0.5 * (D1 + D2 + D3) + 3.0 * S)

        case _:
            raise ValueError(f"Invalid stress dimensions: shape = {stress.T.shape}.")


def interpolateEquivalentStress(nodes, connectivity, LM, dofs, E, nu):
    nnodes, ndim = nodes.shape
    recoveredStress = np.zeros(nnodes)
    ncontributions = np.zeros(nnodes, dtype=np.int64)
    
    elementDofs = dofs[LM]
    
    for inode, ijk in enumerate(np.ndindex(*((2, ) * ndim))):
        rst = tuple(2.0 * i - 1.0 for i in ijk)
        N, dNdr = referenceShapeFunctions(rst)
        
        J = np.einsum("jk, ikl", dNdr, nodes[connectivity])
        dNdx = np.linalg.solve(J, dNdr[np.newaxis, ...])

        B = strainOperator(dNdx)
        C = defaultLinearElasticMaterial(ndim, E, nu)
        
        stress = np.einsum("eik,ek,eij->ej", B, elementDofs, C, optimize=True)
        
        recoveredStress[LM[:, inode]] += equivalentStress(stress)
        ncontributions[LM[:, inode]] += 1
        
    return recoveredStress / ncontributions    


    
def createVtuConnectivity(ncells):
    ids = np.arange(np.sum(np.prod(ncells + 1, axis=1)), dtype=np.int64)
    ndim = ncells.shape[1]
    
    edges = np.empty((2 * (ndim - 1) * np.sum(ncells), 2), dtype=np.int64)
    volumes = np.empty((np.sum(np.prod(ncells, axis=1)), 2**ndim), dtype=np.int64)
    cell0 = np.mgrid[(slice(2), )*ndim].transpose() .reshape(-1, ndim)
    
    def addEdges(nedges, axis, side0, side1=None):
        sides = (side0, side1) if ndim == 3 else (side0, )
        for iedge in range(nedges):
            ijk1 = insertEntry(sides, axis, iedge)
            ijk2 = insertEntry(sides, axis, iedge + 1)
            edges[edgeIndex + iedge] = (elementIds[ijk1], elementIds[ijk2])
        return nedges
    
    edgeIndex, volumeIndex, offset1 = 0, 0, 0
    for ielement, ncell in enumerate(ncells):
        
        offset0, offset1 = offset1, offset1 + np.prod(ncell + 1)
        elementIds = np.reshape(ids[offset0:offset1], ncell + 1)
                
        # Internal cells
        for ijk0 in np.ndindex(*ncell):
            volumes[volumeIndex] = tuple(elementIds[tuple(ijk1)] for ijk1 in (cell0 + ijk0))
            volumeIndex += 1
            
        # Edge cells
        sides3D = ((0, 0), (0, -1), (-1, 0), (-1, -1))
        for sides in sides3D if ndim == 3 else ((0, ), (-1, )):
            for axis in range(ndim):
                edgeIndex += addEdges(ncell[axis], axis, *sides)
                
    assert(edgeIndex == edges.shape[0])
    assert(volumeIndex == volumes.shape[0])
    assert(offset1 == ids.shape[0])
        
    return edges, volumes


vtkDataTypeMap = { np.dtype('int8') : "Int8",  np.dtype('int16') : "Int16", 
                    np.dtype('int32') : "Int32", np.dtype('int64') : "Int64", 
                    np.dtype('float64') : "Float64" }


def writeUnstructuredMesh(filename, points, connectivity, offsets, types, pointdata, datanames):
    def writeAscii( text ):
        outfile = open( filename, "a" )
        outfile.write( text )
        outfile.write( "\n" )
        outfile.close( )
              
    def openNode( name, attributes = { } ):
        attributeString = ''.join( [" "+ key + "=\"" + value + "\"" for key, value in attributes.items( )] )  
        writeAscii( "<" + name + attributeString + ">" )
        return lambda : writeAscii( "</" + name + ">" )
        
    def writeDataSet( data, name=None ):
        attributes = { "format" : "ascii", "type" : vtkDataTypeMap[data.dtype], 
                        "NumberOfComponents" : str(data.shape[1] if data.ndim > 1 else 1) }
        if name != None:
            attributes["Name"] = name
            
        closeDatasetNode = openNode( "DataArray", attributes )
        writeAscii( '\n'.join( map( str, data.ravel() ) ) )
        closeDatasetNode( )

    headerAttributes = { "byte_order" : "LittleEndian",
                          "type"       : "UnstructuredGrid",
                          "version"    : "0.1" }
    
    open( filename, 'w').close( )
    writeAscii( "<?xml version=\"1.0\"?>" )
    closeVtkNode = openNode( "VTKFile", headerAttributes )
    closeGridNode = openNode( "UnstructuredGrid" )
    closePieceNode = openNode( "Piece", { "NumberOfCells"  : str( len( types ) ), 
                                          "NumberOfPoints" : str( points.shape[0] ) } )

    # Write all point data sets    
    closeNode = openNode( "PointData" )
    
    for data, name in zip(pointdata, datanames):
        writeDataSet(data, name)

    closeNode( )
    
    # Write result point coordinates
    closeNode = openNode( "Points" )
    writeDataSet( points )
    closeNode( )

    # Write result cells
    closeNode = openNode( "Cells" )
    writeDataSet( connectivity, "connectivity" )
    writeDataSet( offsets, "offsets" )
    writeDataSet( types, "types" )
    closeNode( )
    
    closePieceNode( )
    closeGridNode( )
    closeVtkNode( )
     
# Number codes for vertices, edges, quads and cubes
vtkCellTypeMap = { 0 : 1, 1 : 3, 2 : 8, 3 : 11 }

def writeVtu(filename, nodes, connectivity, **kwargs):
    xyz = np.concatenate([nodes, np.zeros((nodes.shape[0], 3 - nodes.shape[1]))], axis=1)
    offsets = np.arange(0, connectivity.size, connectivity.shape[1]) + connectivity.shape[1]
    types = np.full(connectivity.shape[0], vtkCellTypeMap[nodes.shape[1]])
    writeUnstructuredMesh(filename, xyz, connectivity.ravel(), offsets, types, kwargs.values(), kwargs.keys())
