import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as mcolors
from matplotlib import cm
import time

#from concurrent.futures import ProcessPoolExecutor
#import multiprocessing

import zugversuch

def createCircle(nelements: int, 
                 origin: list[float] = (0.0, 0.0),
                 radius: list[float] = 1.0): 
    nelementsR = int(0.36 * nelements)
    nelements0 = nelements - nelementsR
    nelements1 = nelements - nelementsR
    blend = 0.38
    
    def shape(squareIndex):
        if squareIndex == 0: return nelements0, nelements1
        if squareIndex == 1 or squareIndex == 3: return nelements0, nelementsR
        if squareIndex == 2 or squareIndex == 4: return nelements1, nelementsR
    
    xyC, rectC = zugversuch.createCartesianGrid(shape(0), (2, 2), (-1, -1))
    xyR0, rectR0 = zugversuch.createCartesianGrid(shape(1), (2, 2), (-1, -1))
    xyR1, rectR1 = zugversuch.createCartesianGrid(shape(2), (2, 2), (-1, -1))
    
    def innerSquare(xy):
        (r, s), (phi1, phi2) = xy.T, (np.pi * xy.T[0] / 4, np.pi * xy.T[1] / 4)
        upper = (1 + s[np.newaxis]) / 2 * np.array([np.sin(phi1) - r / np.sqrt(2), np.cos(phi1)])
        lower = (1 - s[np.newaxis]) / 2 * np.array([np.sin(phi1) - r / np.sqrt(2), -np.cos(phi1)])
        left = (1 - r[np.newaxis]) / 2 * np.array([-np.cos(phi2), np.sin(phi2) - s / np.sqrt(2)])
        right = (1 + r[np.newaxis]) / 2 *  np.array([np.cos(phi2), np.sin(phi2) - s / np.sqrt(2)]) 
        return (blend * (upper + lower + left + right) + (1 - blend) * xy.T).T
    
    def outerSquare(xy, rotate):    
        r, s, phi = xy.T[0], xy.T[1], np.pi * xy.T[0] / 4
        lower = blend * np.array([np.sin(phi), np.cos(phi)]) + (1 - blend) * np.array([r, s * 0 + 1])
        upper = np.array([2 * np.sin(phi), 2 * np.cos(phi)])
        blended = (1 - s[np.newaxis]) / 2 * lower + (1 + s[np.newaxis]) / 2 * upper
        
        for _ in range(rotate):
            blended[0], blended[1] = blended[1].copy(), -blended[0].copy()
        
        return blended.T
    
    nodeOffsets = [0, np.max(rectC) + 1, np.max(rectR0) + 1, np.max(rectR1) + 1]
    nodeOffsets = np.cumsum(nodeOffsets + nodeOffsets[2:])
    
    xyList = [innerSquare(xyC)] + [outerSquare(xyI, i) for i, xyI in enumerate([xyR0, xyR1, xyR0, xyR1])]
    rectList = [rectangle + offset for rectangle, offset in zip([rectC, rectR0, rectR1, rectR0, rectR1], nodeOffsets)]
             
    xy = np.concatenate(xyList, axis=0)            
    xy *= 0.5 * radius
    xy += np.array([origin])
    
    connectivity = np.concatenate(rectList, axis=0)
    cellOffsets = np.cumsum([0] + [len(rectangle) for rectangle in rectList])
    
    def edge(index, axis, side):
        n0, n1 = shape(index)
        if axis == 0: cellIds = np.arange(0, n1, 1) + side * (n0 - 1) * n1
        if axis == 1: cellIds = np.arange(0, n0 * n1, n1) + side * (n1 - 1)
        cellIds += cellOffsets[index]
        return cellIds, slice(2*side, 2*side + 2) if axis == 0 else slice(side, side + 4, 2)
        
    def connect(index0, index1, edge0, edge1, step=1):
        ids0, slice0 = edge(index0, *edge0)
        connectivity[*edge(index1, *edge1)] = connectivity[ids0[::step], slice0][:, ::step]
    
    connect(0, 1, (1, 1), (1, 0), step=1)  # mid    - top
    connect(1, 2, (0, 1), (0, 0), step=1)  # top    - right
    connect(0, 2, (0, 1), (1, 0), step=-1) # mid    - right
    connect(2, 3, (0, 1), (0, 0), step=1)  # right  - bottom
    connect(0, 3, (1, 0), (1, 0), step=-1) # mid    - bottom
    connect(3, 4, (0, 1), (0, 0), step=1)  # bottom - left
    connect(0, 4, (0, 0), (1, 0), step=1)  # mid    - left
    connect(4, 1, (0, 1), (0, 0), step=1)  # top    - left
        
    mask = np.full(nodeOffsets[-1], False)
    mask[connectivity.ravel()] = True
    
    indexMap = np.full(len(mask), -1)
    indexMap[mask] = np.arange(np.sum(mask))
    
    return xy[mask], indexMap[connectivity]

def extrude(xy, connectivity, xticks):
    xyz = np.repeat(xy[np.newaxis, :, (0, 0, 1)], len(xticks), axis=0).reshape((-1, 3))
    xyz[:, 0] = np.repeat(xticks[..., np.newaxis], xy.shape[0], axis=1).ravel()
    
    connectivity = np.concatenate([connectivity, connectivity + xy.shape[0]], axis=1)
    #return xyz, connectivity
    connectivity = np.repeat(connectivity[..., np.newaxis], len(xticks) - 1, axis=2)
    connectivity += np.arange(len(xticks) - 1).reshape((1, 1, -1)) * xy.shape[0]
    
    return xyz, connectivity.transpose((2, 0, 1)).reshape((-1, connectivity.shape[1]))



# ndim = 2
# nelements = (50, ) + (10, ) * (ndim - 1)
# lengths = (5.0, ) + (1.0, ) * (ndim - 1)
# origin = (0, )  + tuple(-l/2 for l in lengths[1:])
    

# xticks = np.linspace(origin[0], origin[0] + lengths[0], nelements[0] + 1)
# xyz, connectivity = extrude(*createCircle(50), xticks)





# connect(rectC, rect0, (0, 1), (1, 0))

# print(rect3)

# nodes1 = np.arange(nelements0 * nelements1).reshape

# xyz = np.concatenate([xyzC, xyz0, xyz1, xyz2, xyz3])
# xyz = np.concatenate([xyzC, xyz0])

# def transform(xyz):
#     y, phi, blendLower = xyz.T[1], np.pi * xyz.T[0] / 4, 0
#     lower = (1 - blendLower) * np.cos(phi) + blendLower
#     dist = (2 - y) / lower + (y - 1) * 2
#     return np.array([np.sin(phi) * dist, np.cos(phi) * dist]).T

# y0 = np.full(100, -1.25)
# xyzR = np.array([y0, np.linspace(1, 2, 100)]).T
# x, y = transform(xyzR).T
# plt.plot(np.sqrt(x**2+y**2))
# v1 = 1 / np.cos(np.pi/4)
# v2 = 2
# y0 = np.linspace(1, 2, 100)
# #plt.plot(np.sqrt(x**2+y**2) - ((1.82842713e+00 + v1 - v2) * y0 - 4.14213587e-01 * y0**2 - 2*v1 + v2))
# #plt.plot(xyzR.T[1], (1.82842713e+00*y -4.14213587e-01*y**2))# / (1.82842713e+00*y -4.14213587e-01*y**2)
# #print(np.polyfit(xyzR.T[1], np.sqrt(x**2+y**2), 10))
# plt.show()

#n = 100
#xyzR = np.array([np.linspace(-1, 1, n), np.full(n, 1)]).T
#plt.plot(*xyzR.T, "x")
# plt.plot(*xyz0.T, "bo")
# plt.plot(*xyz2.T, "bo")
# plt.plot(*xyz1.T, "g.")
# plt.plot(*xyz3.T, "g.")
# plt.plot(*xyzC.T, "rx")

# xyz, connectivity = createCircle(50)
# plt.plot(*xyz.T, "rx")
# plt.axis("equal")
# # plt.ylim(-0.2, 2.2)
# # plt.xlim(-1.2, 1.2)
# plt.show()






if __name__ == "__main__":

    # Materialparameter SS316L
    E = 195.0
    nu = 0.35
    
    rightDisplacement = 0.05
    
    ndim = 3
    
    # Discretization parameters
    nelements = (100, ) + (10, ) * (ndim - 1)
    lengths = (20.0, ) + (1.0, ) * (ndim - 1)
    origin = (0, )  + tuple(-l/2 for l in lengths[1:])
    
    # Location maps and sparse matrix creation
    #nodes, connectivity = zugversuch.createCartesianGrid(nelements, lengths, origin)
    
        
    xticks = np.linspace(origin[0], origin[0] + lengths[0], nelements[0] + 1)
    nodes, connectivity = extrude(*createCircle(15), xticks)

    nnodes = nodes.shape[0]
    ntotal = connectivity.shape[0]
    print(f"Number of elements: {ntotal}\nnumber of dofs: {ndim * nnodes}")
      
    x0, x1 = 0.2 * lengths[0], 0.3 * lengths[0]
    factor = 0.6
    
    x2, x3 = origin[0] + lengths[0] - x1, origin[0] + lengths[0] - x0
    def transformY(x, y):
        t = 1.0
        if x > x0 and x <= x1:
            t = 1.0 - (x - x0) / (x1 - x0)
        elif x > x1 and x <= x2:
            t = 0.0
        elif x > x2 and x <= x3:
            t = (x - x2) / (x3 - x2)    
        return t + (1 - t) * factor
          

    r = np.sqrt(nodes[:, 1]**2 + nodes[:, 2]**2)
    scaling = np.array([transformY(xi, yi) for xi, yi in zip(nodes[:, 0], r)])
    nodes[:, 1:] *= scaling[:, np.newaxis]


    # Prepare Dirichlet dofs
    leftIds = np.nonzero(nodes[:, 0] <= origin[0])[0]
    rightIds = np.nonzero(nodes[:, 0] >= origin[0] + lengths[0])[0]
    
    leftCenterXYZ = [origin[0]] + [o + l/2 for o, l in zip(origin[1:], lengths[1:])]
    leftCenterId = np.argmin(np.sum((nodes - np.array(leftCenterXYZ))**2, axis=1))
    
    rightCenterXYZ = [origin[0] + lengths[0]] + [o + l/2 for o, l in zip(origin[1:], lengths[1:])]
    rightCenterId = np.argmin(np.sum((nodes - np.array(rightCenterXYZ))**2, axis=1))
    
    pointIds = [[leftCenterId + i * nnodes, rightCenterId + i * nnodes] for i in range(1, ndim)]
    dirichletIds = np.concatenate([leftIds, rightIds] + pointIds)

    dirichletValues = np.concatenate([leftIds*0.0, rightIds*0.01 + rightDisplacement, [0.0, 0.0] * (ndim - 1)])
    
    # Assemble
    # with ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
    #     K, F, LM = zugversuch.assembleLinearElasticity(nodes, connectivity, E, nu, pool=executor)
    K, F, LM = zugversuch.assembleLinearElasticity(nodes, connectivity, E, nu)
    
    # Impose boundary conditions and solve system
    zugversuch.imposeDirichlet(K, F, dirichletIds, dirichletValues)
    
    # dofs3D = scipy.sparse.linalg.spsolve(K, F)
    
    def count(*args): count.niter += 1
    count.niter = 0
    t0 = time.time()
    invDiag = 1 / K.diagonal()
    precond = scipy.sparse.linalg.LinearOperator(shape=K.shape, matvec=lambda r: r * invDiag)
    dofs3D = scipy.sparse.linalg.cg(K, F, rtol=1e-10, M=precond, callback=count)[0]
    
    print(f"Solution took {time.time() - t0:.5g} seconds and {count.niter} iterations.", flush=True)
    t0 = time.time()
    
    recoveredStress3D = zugversuch.interpolateEquivalentStress(nodes, connectivity, LM, dofs3D, E, nu)
    
    zugversuch.writeVtu("test.vtu", nodes, connectivity, Solution=dofs3D.reshape(nodes.shape[::-1]).T, Stress=recoveredStress3D)
    
    print(f"Postprocessing took {time.time() - t0:.5g} seconds.", flush=True)
    
    #cellMask = np.any(np.abs(nodes[connectivity][..., 2] - origin[2] + lengths[2] / 2) < 1e-10 * lengths[2], axis=1)
    #nodeMask = np.full(nodes.shape[0], False)
    #nodeMask[connectivity[cellMask].ravel()] = True
    #nodeMap = np.full(nodes.shape[0], -1)
    #nodeMap[nodeMask] = np.arange(np.sum(nodeMask))
    #
    #for ielement in np.where(sliceMask)[0]:
    
    # # Extract plane
    # sliceMask = abs(nodes[:, -1] - (origin[-1] + lengths[-1] / 2)) < 1e-8
    
    # dofs = dofs3D[np.concatenate([sliceMask, sliceMask, sliceMask*False])]
    # recoveredStress = recoveredStress3D[sliceMask]
        
    # # Prepare triangulation
    # dofsX, dofsY = dofs[:len(dofs) // 2], dofs[len(dofs) // 2:]
    # scaling = 0.05 *  max(*lengths) / np.max(np.sqrt(dofsX**2 + dofsY**2))
    
    # nodeIDs = np.arange(np.prod(np.add(nelements, 1)[:2])).reshape(np.add(nelements, 1)[:2])
    # triangles = np.array([[nodeIDs[:-1, :-1], nodeIDs[1:, :-1], nodeIDs[1:, 1:]], 
    #                       [nodeIDs[1:, 1:], nodeIDs[:-1, 1:], nodeIDs[:-1, :-1]]])
    # triangles = triangles.transpose([2, 3, 0, 1]).reshape([-1, 3])
    
    # triangulation = tri.Triangulation(nodes[sliceMask, 0] + scaling * dofsX, nodes[sliceMask, 1] + scaling * dofsY, triangles)
    
    # # Prepare bounds
    # boundsX = min(np.min(triangulation.x), origin[0]), max(np.max(triangulation.x), origin[0] + lengths[0])
    # boundsY = min(np.min(triangulation.y), origin[1]), max(np.max(triangulation.y), origin[1] + lengths[1])
    # sbounds = (0, np.max(recoveredStress3D))
    
    # # Plot equivalent stresses
    # aspect = (boundsY[1] - boundsY[0]) / (boundsX[1] - boundsX[0])
    # fig = plt.figure(figsize=(12, int(12 * aspect + 2)))
    # ax = fig.add_subplot(111)
    
    # tricf = ax.tricontourf(triangulation, recoveredStress.ravel(), cmap=cm.turbo, vmin=sbounds[0], vmax=sbounds[1])
    
    # # Plot original bounding box
    # ax.plot([origin[0], origin[0], origin[0] + lengths[0], origin[0] + lengths[0], origin[0]],
    #         [origin[1], origin[1] + lengths[1], origin[1] + lengths[1], origin[1], origin[1]], 'b--')
    
    # # Plot limits and scaling
    # plt.xlim([boundsX[0] - 0.05 * (boundsX[1] - boundsX[0]), boundsX[1] + 0.05 * (boundsX[1] - boundsX[0])])
    # plt.ylim([boundsY[0] - 0.05 * (boundsY[1] - boundsY[0]), boundsY[1] + 0.05 * (boundsY[1] - boundsY[0])])
    # ax.axis('equal')
    
    # # Colorbar that shows entire color range since it's not attached to previously drawn artist
    # norm = mcolors.Normalize(vmin=sbounds[0], vmax=sbounds[1])
    # mappable = cm.ScalarMappable(norm=norm, cmap=cm.turbo)
    # cbar = fig.colorbar(mappable, ax=ax, orientation="horizontal", pad=0.1, aspect=40)
    
    # plt.show()
