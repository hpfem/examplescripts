from paraview.simple import *
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np

# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

timeSteps = 96
outputData = np.zeros((timeSteps, 5))

for i in range(timeSteps):

    vtkFile = XMLUnstructuredGridReader(registrationName='test', FileName=['C:\\Users\\Philipp\\Desktop\\application\\cpp\\build\\bin\\outputs\\calibration_{0}.vtu'.format(i)])
	
    dataset = servermanager.Fetch(vtkFile)
    outputData[i,0] = np.max(dataset.GetPointData().GetArray("Solution"))

    # create a new 'IsoVolume'
    isoVolume1 = IsoVolume(registrationName='IsoVolume1', Input=vtkFile)
    isoVolume1.InputScalars = ['POINTS', 'Solution']
    isoVolume1.ThresholdRange = [1350.0, float("inf")]

    dataset = paraview.servermanager.Fetch(isoVolume1)
    points = dsa.WrapDataObject(dataset).Points
    outputData[i, 1:4] = [np.max(xi) - np.min(xi) for xi in points.T] if points is not None else [0.0] * 3
            
    # create a new 'IntegrateVariables'
    integrateVariables1 = IntegrateVariables(registrationName='IntegrateVariables1', Input=isoVolume1)

    dataset = paraview.servermanager.Fetch(integrateVariables1)
    integrated_volume = dataset.GetCellData().GetArray('Volume')
    outputData[i, 4] = integrated_volume.GetValue(0) if integrated_volume is not None else 0.0

    # destroy filters
    Delete(integrateVariables1)
    Delete(isoVolume1)
    Delete(vtkFile)
    
print('Temp      Length   Width    Depth   Volume')
for T, MPx, MPy, MPz, MPV in outputData:
    print(f"{T:<9.2f} {1000*MPx:<8.2f} {1000*MPy:<8.2f} {1000*MPz:<7.2f} {MPV:.3e}")
    
#np.savetxt("C:\\Users\\Philipp\\Desktop\\application\\cpp\\build\\bin\\outputs\\outputData1.csv", outputData)