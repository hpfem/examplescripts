from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

# cat ~/log/output.out | grep -A 5 This | grep slabs
nslabs = [108, 165, 165, 166, 166, 167, 167, 167, 167, 168, 168, 167, 148, 148, 148, 149, 149, 149, 150, 150, 150, 151, 151, 152, 152, 152, 150, 151, 151, 151, 151, 150, 150, 150, 150, 150, 150, 150, 150, 151, 150, 171, 171, 170, 170, 170, 170, 185, 188, 189, 190, 191, 191, 209, 210, 211, 213, 214, 213, 198, 197, 196, 196, 194, 163, 162, 162, 162, 161, 161, 161, 161, 161, 161, 161, 161, 160, 161, 160, 161, 160, 161, 160, 161, 160, 161, 161, 161, 161, 161, 161, 162, 162, 162, 161, 162, 162, 164, 164, 163, 163, 163, 163, 163, 164, 200, 201, 199, 198, 196, 195, 193, 192, 190, 188, 185, 174]

# cat ~/log/output.out | grep -A 5 This | grep simulation
realtime = [0.107029, 0.164587, 0.164838, 0.165624, 0.165697, 0.166134, 0.166208, 0.166588, 0.166707, 0.167229, 0.16759, 0.16656, 0.147006, 0.14737, 0.147858, 0.148232, 0.148639, 0.148982, 0.149278, 0.149703, 0.14996, 0.150496, 0.150527, 0.15115, 0.151453, 0.151228, 0.149955, 0.150013, 0.15016, 0.150014, 0.150184, 0.149939, 0.14998, 0.149915, 0.14976, 0.149939, 0.149559, 0.149974, 0.149626, 0.150044, 0.149969, 0.170501, 0.170015, 0.169766, 0.169397, 0.169332, 0.169152, 0.184747, 0.187025, 0.188266, 0.189489, 0.190672, 0.190962, 0.2083, 0.209387, 0.210939, 0.21213, 0.21352, 0.212724, 0.197442, 0.196175, 0.195636, 0.19543, 0.193887, 0.162304, 0.161768, 0.161505, 0.161199, 0.160934, 0.160829, 0.160751, 0.160482, 0.160545, 0.160277, 0.160178, 0.160199, 0.159884, 0.160129, 0.159715, 0.160157, 0.159673, 0.160161, 0.159751, 0.160084, 0.159858, 0.160019, 0.160104, 0.160086, 0.16034, 0.160302, 0.160535, 0.16111, 0.16106, 0.161083, 0.16084, 0.161614, 0.161293, 0.163149, 0.163054, 0.1627, 0.162356, 0.162324, 0.162092, 0.162645, 0.163488, 0.199023, 0.200211, 0.198725, 0.197256, 0.195977, 0.194411, 0.192947, 0.191257, 0.189755, 0.187548, 0.184957, 0.17338]

beginLayer = 0
endLayer = 117 #len(nslabs)

vtupath = 'E:\\data\\partscale\\hofeim'
vtubase = 'metal_boundary'

# ffmpeg -framerate 60 -i png/layers_%4d.png -c:v libx264 -preset slow -crf 18 -pix_fmt yuv420p layers.mp4
pngname = 'E:\\data\partscale\\test\\layers_'

resolution = [1920, 500]
colorScale = [25.0, 1000.0]

slabOffsets = [0]
timeOffsets = [0.0]

for nslab in nslabs:
    slabOffsets.append(slabOffsets[-1] + nslab)
    
for duration in realtime:
    timeOffsets.append(timeOffsets[-1] + duration)

for ilayer in range(beginLayer, endLayer):
    layerFiles = [vtupath + f'\\layer_{ilayer}\\' + vtubase + f'_{ifile + 1}.vtu' 
        for ifile in range(slabOffsets[ilayer], slabOffsets[ilayer] + nslabs[ilayer])]
    
    # create a new 'XML Unstructured Grid Reader'
    vtusource = XMLUnstructuredGridReader(registrationName='vtusource', FileName=layerFiles)
    
    # Properties modified on vtusource
    vtusource.TimeArray = 'None'
    
    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    renderView1.OrientationAxesVisibility = 0
    
    # show data in view
    vtusourceDisplay = Show(vtusource, renderView1, 'UnstructuredGridRepresentation')
    
    # trace defaults for the display properties.
    vtusourceDisplay.Representation = 'Surface'
    
    # update the view to ensure updated data information
    renderView1.Update()
    
    # set scalar coloring
    ColorBy(vtusourceDisplay, ('POINTS', 'Solution'))
    
    # rescale color and/or opacity maps used to include current data range
    vtusourceDisplay.RescaleTransferFunctionToDataRange(True, False)
    
    # show color bar/color legend
    vtusourceDisplay.SetScalarBarVisibility(renderView1, True)
    
    # get color transfer function/color map for 'Solution'
    solutionLUT = GetColorTransferFunction('Solution')
    solutionLUT.RescaleTransferFunction(colorScale[0], colorScale[1])
    
    # get opacity transfer function/opacity map for 'Solution'
    solutionPWF = GetOpacityTransferFunction('Solution')
    solutionPWF.RescaleTransferFunction(colorScale[0], colorScale[1])
    
    # get 2D transfer function for 'Solution'
    solutionTF2D = GetTransferFunction2D('Solution')
    solutionTF2D.RescaleTransferFunction(colorScale[0], colorScale[1], 0.0, 1.0)
    
    # get color legend/bar for solutionLUT in view renderView1
    solutionLUTColorBar = GetScalarBar(solutionLUT, renderView1)
    solutionLUTColorBar.WindowLocation = 'Lower Center'
    solutionLUTColorBar.Orientation = 'Horizontal'
    solutionLUTColorBar.ScalarBarLength = 0.15
    solutionLUTColorBar.Title = 'Temperature'
    solutionLUTColorBar.AutomaticLabelFormat = 0
    solutionLUTColorBar.RangeLabelFormat = '%.0f'
    solutionLUTColorBar.LabelFormat = '%.0f'
    
    # Annotate layer
    annotateTimeFilterLayer = AnnotateTimeFilter(registrationName='annotateTimeFilterLayer', Input=vtusource)
    annotateTimeFilterLayer.Format = f'Layer: {ilayer + 1}'
    annotateTimeFilterLayerDisplay = Show(annotateTimeFilterLayer, renderView1, 'TextSourceRepresentation')
    
    # Annotate time
    annotateTimeFilterTime = AnnotateTimeFilter(registrationName='annotateTimeFilterTime', Input=vtusource)
    annotateTimeFilterTime.Format = 'Time: {time:.2f}s'
    annotateTimeFilterTime.Shift = timeOffsets[ilayer]
    annotateTimeFilterTime.Scale = realtime[ilayer] / nslabs[ilayer]
    annotateTimeFilterTimeDisplay = Show(annotateTimeFilterTime, renderView1, 'TextSourceRepresentation')
    
    # Annotate slab
    annotateTimeFilterSlab = AnnotateTimeFilter(registrationName='annotateTimeFilterSlab', Input=vtusource)
    annotateTimeFilterSlab.Format = 'Slab: {time:.0f}'
    annotateTimeFilterSlab.Shift = slabOffsets[ilayer]
    annotateTimeFilterSlab.Scale = 1
    annotateTimeFilterSlabDisplay = Show(annotateTimeFilterSlab, renderView1, 'TextSourceRepresentation')
    
    renderView1.Update()
    
    annotateTimeFilterLayerDisplay.WindowLocation = 'Any Location'
    annotateTimeFilterLayerDisplay.Position = [0.01, 0.95]
    annotateTimeFilterTimeDisplay.WindowLocation = 'Any Location'
    annotateTimeFilterTimeDisplay.Position = [0.01, 0.90]
    annotateTimeFilterSlabDisplay.WindowLocation = 'Any Location'
    annotateTimeFilterSlabDisplay.Position = [0.01, 0.85]
    
    # get animation scene
    animationScene1 = GetAnimationScene()
    animationScene1.PlayMode = 'Snap To TimeSteps'
    
    # get layout
    layout1 = GetLayout()
    layout1.SetSize(*resolution)
    
    # current camera placement for renderView1
    renderView1.CameraPosition = [0.0, -20.922688742405775, 18.12699379247124]
    renderView1.CameraFocalPoint = [0.0, 49.60545110006296, -50.2671838565589]
    renderView1.CameraViewUp = [0.0, 0.6961619973355495, 0.7178846265715542]
    renderView1.CameraParallelScale = 25.459566121260124
    
    animationScene1.GoToFirst()
    
    for i in range(nslabs[ilayer]):
        SaveScreenshot(pngname + f'{slabOffsets[ilayer] + i:04d}.png', renderView1, ImageResolution=resolution)
        animationScene1.GoToNext()
    
    ResetSession()